#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "vocr" for configuration "Release"
set_property(TARGET vocr APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(vocr PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/vocr/libvocr.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS vocr )
list(APPEND _IMPORT_CHECK_FILES_FOR_vocr "${_IMPORT_PREFIX}/lib/vocr/libvocr.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
