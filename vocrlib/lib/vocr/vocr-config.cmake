
get_filename_component(SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)

if (ANDROID_NDK_ABI_NAME)
    include(${SELF_DIR}/${ANDROID_NDK_ABI_NAME}/vocr.cmake)
else()
    include(${SELF_DIR}/vocr.cmake)
endif()