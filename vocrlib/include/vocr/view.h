//
// Created by chingy on 12/10/17.
//

#ifndef VOCR_VISDOC_VIEW_H
#define VOCR_VISDOC_VIEW_H

#include <algorithm>
#include <memory>
#include <opencv2/core/types.hpp>
#include <vector>
#include <vocr/interface.h>
#include <vocr/ocr_params.h>
#include <vocr/stdx.h>

namespace vdoc {

/**
 * A base class for UI elements (such as layouts, text/image fields)
 */
struct View : public std::enable_shared_from_this<View> {
  protected:
    /**
     * A parent view object
     */
    std::weak_ptr<vdoc::View> parent_;

    /**
     * vector of sub-view elements
     */
    std::vector<std::shared_ptr<vdoc::View>> children_;

    /**
     * sets the margin area on the left side of an element (%)
     */
    double left_;

    /**
     * sets the margin area on the top of an element (%)
     */
    double top_;

    /**
     * sets the margin area on the right side of an element (%)
     */
    double right_;

    /**
     * sets the margin area on the bottom of an element (%)
     */
    double bottom_;

    /**
     * x offset value (px)
     */
    int x_;

    /**
     * y offset value (px)
     */
    int y_;

    /**
     * width of the view
     */
    int width_;

    /**
     * height of the view
     */
    int height_;

    /**
     * *** OCR parameters ***
     */
    OcrParams ocr_;

    /**
     * Only active fields are added to the VisDoc when creating
     */
    bool active_ = true;

  public:
    // View() = default;

    View() : children_({}), x_(0), y_(0), width_(0), height_(0), left_(0), top_(0), right_(0), bottom_(0) {}

    View(const int &x, const int &y, const int &width, const int &height, const double &left = 0.0,
         const double &top = 0.0, const double &right = 0.0, const double &bottom = 0.0)
        : children_({}), x_(x), y_(y), width_(width), height_(height) {
        setMargin(left, top, right, bottom);
    }

    void setMargin(const double &left_, const double &top_, const double &right_, const double &bottom_) {
        if (left_ < right_) {
            setMarginLeft(left_);
            setMarginRight(right_);
        } else {
            setMarginRight(right_);
            setMarginLeft(left_);
        }

        if (top_ < bottom_) {
            setMarginTop(top_);
            setMarginBottom(bottom_);
        } else {
            setMarginBottom(bottom_);
            setMarginTop(top_);
        }
    }

    double getMarginLeft() const { return left_; }

    void setMarginLeft(const double &left_) { View::left_ = std::min(std::max(0.0, left_), (1.0 - View::right_)); }

    double getMarginTop() const { return top_; }

    void setMarginTop(const double &top_) { View::top_ = std::min(std::max(0.0, top_), 1.0 - View::bottom_); }

    double getMarginRight() const { return right_; }

    void setMarginRight(const double &right_) { View::right_ = std::min(std::max(0.0, right_), 1.0 - View::left_); }

    double getMarginBottom() const { return bottom_; }

    void setMarginBottom(const double &bottom_) { View::bottom_ = std::min(std::max(0.0, bottom_), 1.0 - View::top_); }

    /**
     * X coord can be expressed as parent X coord + parent margin (null parent is defined implicitly) on X + X coord of
     * the object itself
     */

    int getX() const {
        std::shared_ptr<View> parent = parent_.lock();

        return static_cast<int>((parent != nullptr) ? parent->getX() + x_ + parent->getWidth() * left_ : x_);
    }

    void setX(const int x_) { View::x_ = x_; }

    /**
     * Y coord can be expressed as parent Y coord + parent margin (null parent is defined implicitly) on Y + Y coord of
     * the object itself
     */

    int getY() const {
        std::shared_ptr<View> parent = parent_.lock();

        return static_cast<int>((parent != nullptr) ? parent->getY() + y_ + parent->getHeight() * top_ : y_);
    }

    void setY(const int y_) { View::y_ = y_; }

    int getWidth() const {
        std::shared_ptr<View> parent = parent_.lock();

        return (parent != nullptr && parent->getWidth() != 0)
                   ? std::max(0, std::min(width_, (int)(parent->getX() + parent->getWidth() -
                                                        parent->getWidth() * right_ - getX())))
                   : width_;
    }

    void setWidth(const int width_) { View::width_ = std::max(0, width_); }

    int getHeight() const {
        std::shared_ptr<View> parent = parent_.lock();

        return (parent != nullptr && parent->getHeight() != 0)
                   ? std::max(0, std::min(height_, (int)(parent->getY() + parent->getHeight() -
                                                         parent->getHeight() * bottom_ - getY())))
                   : height_;
    }

    void setHeight(const int height_) { View::height_ = std::max(0, height_); }

    cv::Size getSize() const { return cv::Size(getWidth(), getHeight()); }

    void setSize(cv::Size size_) {
        View::width_ = std::max(0, size_.width);
        View::height_ = std::max(0, size_.height);
    }

    cv::Rect getRect() const { return cv::Rect(getX(), getY(), getWidth(), getHeight()); }

    void setRect(const cv::Rect rect_) {
        View::x_ = rect_.x;
        View::y_ = rect_.y;
        View::width_ = std::max(0, rect_.width);
        View::height_ = std::max(0, rect_.height);
    }

    std::weak_ptr<vdoc::View> getParent() const { return parent_; }

    void setParent(std::shared_ptr<vdoc::View> parent_) { View::parent_ = parent_; }

    void addChild(std::shared_ptr<vdoc::View> child_) {
        child_->setParent(shared_from_this());
        children_.push_back(child_);
        inheritOcrParams(shared_from_this(), child_);
    }

    virtual void attachTo(std::shared_ptr<vdoc::View> parent_) { parent_->addChild(shared_from_this()); }

    void inheritOcrParams(const std::shared_ptr<vdoc::View> &from, std::shared_ptr<vdoc::View> &to) {

        to->setOcrParams(from->ocrParams());
    }

    void detach() { View::parent_.reset(); }

    /**
     * return the list of children view entities
     * @param type_
     */
    template <typename T> std::vector<std::shared_ptr<T>> getChildren(int type_ = VDOC_VIEW_CHILDREN_IMMEDIATE) const {

        std::vector<std::shared_ptr<T>> children;

        if (type_ == VDOC_VIEW_CHILDREN_IMMEDIATE || type_ == (VDOC_VIEW_INSTANCE | VDOC_VIEW_CHILDREN_IMMEDIATE)) {
            for (auto child_ : children_) {
                std::shared_ptr<T> child = std::dynamic_pointer_cast<T>(child_);
                if (child == nullptr)
                    continue;
                children.push_back(child);
            }
        } else if (type_ == VDOC_VIEW_CHILDREN_TRAVERSAL ||
                   type_ == (VDOC_VIEW_INSTANCE | VDOC_VIEW_CHILDREN_TRAVERSAL)) {
            for (auto child_ : children_) {
                std::shared_ptr<T> child = std::dynamic_pointer_cast<T>(child_);
                if (child != nullptr)
                    children.push_back(child);
                std::vector<std::shared_ptr<T>> sub = child_->getChildren<T>(type_);
                children.insert(children.end(), sub.begin(), sub.end());
            }
        }
        return children;
    }

    template <typename F, typename Tuple> void apply_function(F foo_, Tuple tuple_, int type_ = VDOC_VIEW_INSTANCE) {

        std::vector<std::shared_ptr<vdoc::View>> children = getChildren<vdoc::View>(type_);

        for (std::shared_ptr<vdoc::View> child : children) {
            stdx::apply(foo_, std::tuple_cat(std::make_tuple(child), tuple_));
        }

        if (type_ % 2 == VDOC_VIEW_INSTANCE) {
            stdx::apply(foo_, std::tuple_cat(std::make_tuple(shared_from_this()), tuple_));
        }
    }

    virtual void apply(class VisDocProcessor *processor){};

    OcrParams &ocrParams() { return ocr_; }

    void setOcrParams(const OcrParams &ocr_params) { View::ocr_ = ocr_params; }

    void setActive(bool val) { active_ = val; }

    bool isActive() const { return active_; }
};

} // namespace vdoc

#endif // VOCR_VIEW_H
