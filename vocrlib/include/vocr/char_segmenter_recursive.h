//
// Created by chingy on 2/16/19.
//

#ifndef VOCR_CHAR_SEGMENTER_RECURSIVE_H
#define VOCR_CHAR_SEGMENTER_RECURSIVE_H

#include "char_segmenter_basic.h"
#include "char_segmenter_interface.h"
#include "image_proc_interface.h"
#include <stack>

namespace vdoc {
class CharSegmenterRecursive : public CharSegmenterDigitsBasic {
  public:
    explicit CharSegmenterRecursive(const OcrParams &params);

    std::vector<cv::Rect2i> segment(const cv::Mat &txt_line_img) override;

    cv::Size referenceSize() override;

  private:
    int space_width_ = 0;
    int avg_width_ = 0;
    std::vector<cv::Rect> splitLongWidthBoxes(const std::vector<cv::Rect> &boxes);
};

class RecursiveAdaThreshCharDetector : public SimpleCharDetector {
  public:
    using SimpleCharDetector::SimpleCharDetector;

    void process(const cv::Mat &src) override;

    cv::Mat processed() override;

    std::vector<cv::Rect> rois() override;

    std::vector<cv::RotatedRect> fineRois() override;

  private:
    int translated_x_ = 0;

    std::vector<cv::Rect> separateCharsVaryingThreshold(const cv::Mat &patch);
    cv::Mat cropPatch(const cv::Mat &src, const cv::Rect &roi);
    void translateToOrigin(std::vector<cv::Rect> &char_boxes);
    bool isLikeChar(const cv::Rect &bbox);
};
} // namespace vdoc

#endif // VOCR_CHAR_SEGMENTER_RECURSIVE_H
