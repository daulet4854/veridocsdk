#ifndef VOCR_VISDOC_IMAGE_VIEW_H
#define VOCR_VISDOC_IMAGE_VIEW_H

#include "view.h"
#include <opencv2/core/mat.hpp>

namespace vdoc {

/**
 * A view component, responsible for displaying external image resources
 */
struct ImageView : public View {
  protected:
    /**
     * A visual representation of an ImageView
     */
    cv::Mat image_;

  public:
    ImageView(const cv::Mat &image, const int &x = 0, const int &y = 0, const double &left = 0.0,
              const double &top = 0.0, const double &right = 0.0, const double &bottom = 0.0) {
        children_ = {};
        image_ = image;
        x_ = x;
        y_ = y;
        width_ = image.cols;
        height_ = image.rows;

        setMargin(left, top, right, bottom);
    }

    ImageView(const int &x, const int &y, const double &left = 0.0, const double &top = 0.0, const double &right = 0.0,
              const double &bottom = 0.0) {
        children_ = {};
        x_ = x;
        y_ = y;
        width_ = 0;
        height_ = 0;

        setMargin(left, top, right, bottom);
    }

    ImageView(const double &left = 0.0, const double &top = 0.0, const double &right = 0.0,
              const double &bottom = 0.0) {
        children_ = {};
        width_ = 0;
        height_ = 0;

        setMargin(left, top, right, bottom);
    }

    /**
     * Returns the intrinsic view image with respect to the size of the parent view
     * @return cv::Mat
     */
    const cv::Mat getImage() const {
        std::shared_ptr<View> parent = parent_.lock();

        if (parent == nullptr)
            return ImageView::image_(cv::Rect(0, 0, width_, height_));

        int _x, _y, _w, _h;
        _x = std::max(0, parent->getX() - getX());
        _y = std::max(0, parent->getY() - getY());
        _w = std::max(0, getWidth() - _x);
        _h = std::max(0, getHeight() - _y);

        if (_w == 0 || _h == 0) {
            return cv::Mat(0, 0, ImageView::image_.type());
        }

        return ImageView::image_(cv::Rect(_x, _y, _w, _h));
    }

    /**
     * Set current @class{cv::Mat} object
     * @param &image_
     */
    void setImage(const cv::Mat &image_) {
        ImageView::image_ = image_;
        // reset view size according to mat boundaries
        View::setSize(cv::Size(image_.cols, image_.rows));
    }

    void attachTo(std::shared_ptr<vdoc::View> parent_) {
        View::attachTo(parent_);

        if (!ImageView::image_.empty())
            return;
        // downcasting the argument to derived class and retrieve the Mat object
        cv::Mat src = std::dynamic_pointer_cast<vdoc::ImageView>(parent_)->getImage();

        // define the cropping region
        int _x = (int)(src.cols * left_) + std::max(0, x_);
        int _y = (int)(src.rows * top_) + std::max(0, y_);
        width_ = (width_ > 0) ? std::min(width_, src.cols) : src.cols;
        height_ = (height_ > 0) ? std::min(height_, src.rows) : src.rows;

        ImageView::setImage(src(cv::Rect(_x, _y, getWidth(), getHeight())));
    }

    /**
     * Emptify intrinsic @class{cv::Mat} object
     */
    void release() { ImageView::image_.release(); }
};

} // namespace vdoc

#endif // VOCR_IMAGE_VIEW_H