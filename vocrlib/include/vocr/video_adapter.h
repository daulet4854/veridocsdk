#include <utility>

//
// Created by chingy on 1/5/18.
//

#ifndef VOCR_VIDEOADAPTER_H
#define VOCR_VIDEOADAPTER_H

#include <chrono>
#include <memory>
#include <opencv2/core/mat.hpp>

#include <vocr/exception/document_not_found_exception.h>
#include <vocr/grabber.h>
#include <vocr/grabber_borders.h>
#include <vocr/grabber_smart.h>

namespace vdoc {

struct Record {

    explicit Record() = default;

    explicit Record(const int &_id, const int &_votes, std::shared_ptr<grab::GrabbingResult> &_result,
                    const cv::Mat &_img, const double &_blur_val)
        : id(_id), votes(_votes), result(_result), original_image(_img), blur_val(_blur_val) {}

    int id = -1;
    int votes = 0;
    double blur_val = 0;
    std::shared_ptr<grab::GrabbingResult> result;
    cv::Mat original_image;
};

struct Frame {

    Frame() = default;

    Frame(const cv::Mat &_img, double _blur_value, int _id) : image(_img), blur_value(_blur_value), id(_id) {}

    cv::Mat image;
    double blur_value;
    int id;
};

class VideoAdapter {

  private:
    /**
     * Buffer for the blur filter
     */
    std::vector<Frame> frames_buffer_;
    int buffer_size_ = 3;

    // Thersholds
    const std::chrono::milliseconds max_detection_time_ = std::chrono::milliseconds(20000);
    const std::chrono::milliseconds max_blurry_time_ = std::chrono::milliseconds(10000);
    const int blur_min_ = 12;
    const int blur_enough_ = 18;
    const int blur_good_ = 25;
    const int success_threshold_ = 2;
    const int grab_fail_thresh_ = 2;

    // Counters
    int grab_fail_counter_ = 0;

    // Stores time point when next_image was first called (until resetting)
    std::chrono::high_resolution_clock::time_point detection_start_time_;

    // Stores approximate time of the beginning of previous image processing.
    // So if current image is blurry, we increase 'blurry_time_sum_' by time gap between two images
    std::chrono::high_resolution_clock::time_point blurry_start_time_;
    std::chrono::milliseconds blurry_time_sum_ = std::chrono::milliseconds::zero();

    /**
     * Indicates whether extraction step is performed
     */
    bool extracted_ = false;

    /**
     * Indicates whether it is required to add original image used for document recognition to results
     * of recognition (as a base64 string)
     */
    bool is_original_image_required_ = false;

    /**
     * Saved result as a JSON String
     */
    std::string json_ = "";

    /**
     * Saved result as a VisualDocument object
     */
    std::shared_ptr<VisualDocument> visdoc_;

    /**
     * If true, then recognition step performed automatically.
     */
    bool auto_mode_ = true;

    /**
     * The expected type of the document to capture and recognize
     */
    DocType type_;

    /**
     * The Grabber
     */
    std::shared_ptr<grab::Grabber> grabber_;

    /**
     * Current best record
     */
    Record best_record_;

    /**
     * Last grabbing result
     */
    std::shared_ptr<grab::GrabbingResult> last_grabbing_result_;

    /**
     * Collection of the records saved so far.
     */
    std::vector<Record> records_;

    /**
     * Used for 'setImage()' method
     */
    cv::Mat recognition_image_;

    /**
     * Saves the provided 'GrabbingResult' in the internal buffer to keep track of grabbing process and choose the best
     * option when necessary.
     * @param result - GrabbingResult
     * @param id - Id of the image (provided by the client)
     * @param img - original image used for grabbing
     */
    void recordResult(std::shared_ptr<grab::GrabbingResult> &result, const int &id, const cv::Mat &img,
                      const double &blur_val);

    /**
     * Indicates whether there is available good image for recognition in the 'records'.
     * Used internally in the 'nextImage()' method to know when to start recognition or signal the client
     * to stop sending images and invoke 'extract()' method
     * @return
     */
    bool isReadyForRecognition();

    /**
     * The Filter through which only 'good enough' images pass. It has a buffer that can keep previous images and
     * output the best one. The filter also determines whether image is good enough for grabbing and/or for extraction.
     * If an image is good for grabbing, that means the result of grabbing can be used for the visualization purposes.
     * If the sharpness values of some image sequence are increasing, then the filter does not output any image for
     * processing, because
     * @param in_img - input image
     * @param out_img - output image. It is not necessarily the same input image. It can be one of the previous images.
     * @param grab
     * @param extract
     */
    virtual void blurFilter(const int &in_id, const cv::Mat &in_img, int &out_id, cv::Mat &out_img, bool &grab,
                            bool &extract, double &blur_val);

    /**
     * Returns true if each image in the collection are sharper than the previous one
     * @param frames - collecation of frames
     * @return bool
     */
    bool isSharpeningSequence(std::vector<Frame> frames);

    /**
     * Returns the image with a highest blur value (sharpest image)
     * @param frames
     * @return
     */
    Frame getSharpestImage(std::vector<Frame> frames);

  public:
    /**
     * Constructor
     * @param type - Document Type
     */
    VideoAdapter(const DocType &type);

    /**
     * Constructor
     * @param type - Document Type
     * @param is_auto - Performs automatic recognition when appropriate frame is caught. Default is True.
     */
    VideoAdapter(const DocType &type, bool is_auto);

    VideoAdapter(const DocType &type, bool is_auto, bool is_original_image_required);

    /**
     *  Set to True recognize documents automatically after successful grabbing.
     *  If False then extract() method must be called manually to perform recognition.
     * @param is_auto - value
     */
    virtual void setAutoMode(bool is_auto);

    /**
     * Main method that accepts and processes each successive image from some source (camera, video record, etc)
     * @param img
     * @param id
     * @return
     */
    virtual bool nextImage(const int &id, const cv::Mat &img);

    /**
     *
     * @param type
     */
    virtual void setDocType(DocType doc_type);

    /**
     *
     * @param type
     * @param archetype
     */
    virtual void receiveDocType(DocType &type);

    /**
     * Initilization method (not in use currently).
     */
    virtual void init();

    /**
     * Returns the ID of the best image so far (not in use currently).
     * @return
     */
    virtual int getCurrentBestImageId();

    /**
     * Returns the best image so far (not in use currently).
     * @return
     */
    virtual cv::Mat getCurrentBestImage();

    /**
     * Returns the confidence score of the best image so far (not in use currently).
     * @return
     */
    virtual float getCurrentConfidenceScore();

    /**
     * Performs recognition of the Document on the current best available image.
     */
    virtual bool extract();

    /**
     * Set image for recognition step. Useful if you want to perform recognition on a higher
     * resolution than the images used for capturing. (Mainly for WEB version of the Veridoc)
     * @param id - ID of an image.
     * @param img
     */
    virtual bool setImage(int id, const cv::Mat &img);

    /**
     * Returns recognition result (after invocation of 'extract()') as a JSON string
     * @param id
     * @return
     */
    virtual std::string getJsonResult();

    /**
     * Returns recognition result (after invocation of 'extract()') as a VisualDocument
     * @return
     */
    virtual std::shared_ptr<VisualDocument> getResult();

    /**
     * Returns the detected regions on the provided images. Results are updated with each passed image.
     * Coordinates in the 'Region' objects correspond to the original images size.
     * @return
     */
    virtual std::vector<grab::Region> getRegions();

    virtual void setIsOriginalImageRequired(bool is_required);

    /**
     * Resets all shared variables and buffers, and leaves the current instance of the VideoAdapter in a state ready
     * for the next recognition.
     */
    virtual void reset();

    virtual ~VideoAdapter() {}
};
} // namespace vdoc

#endif // VOCR_VIDEOADAPTER_H
