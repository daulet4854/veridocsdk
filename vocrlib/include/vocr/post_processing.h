#ifndef VOCR_POST_PROCESSING_H
#define VOCR_POST_PROCESSING_H

#include "visual_document.h"
#include <vector>
#include <vocr/rule_interface.h>

namespace vdoc {

/**
 * Post-processing object
 */
class PostProcessing {
  private:
    // All rules of PostProcessing are stored here
    std::vector<std::shared_ptr<vdoc::RuleInterface>> rules_;

  public:
    PostProcessing();

    /**
     * Apply all rules in `rules_` vector (in order) to visual document
     *
     * @param vis_doc visual document where to apply rules
     */
    void applyRules(std::shared_ptr<vdoc::VisualDocument> &vis_doc);
};
} // namespace vdoc

#endif // VOCR_POST_PROCESSING_H
