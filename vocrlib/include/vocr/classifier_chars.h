#ifndef VOCR_CLASSIFIER_OF_CHARS_H
#define VOCR_CLASSIFIER_OF_CHARS_H

#include <opencv2/dnn.hpp>
#include <vocr/classifier_chars.h>
#include <vocr/classifier_interface.h>
#include <vocr/exception/invalid_argument_exception.h>
#include <vocr/interface.h>

namespace vdoc {
class ClassifierChars : public ClassifierInterface {
  protected:
    /**
     * General model that can classify all symbols
     */
    static cv::dnn::Net net_;

    /**
     * The model that can classify digits only
     */
    static cv::dnn::Net net_digits_;

    /**
     * The model that can classify cyrillic only
     */
    static cv::dnn::Net net_cyr_;

    /**
     * The model that can classify cyrillic only
     */
    static cv::dnn::Net net_mrz_;

    /**
     * Input blob for models. Initialized before prediction.
     */
    cv::Mat blob_;

    std::map<int, std::string> chars_classes_ = {
        {0, "0"},  {1, "1"},  {2, "2"},  {3, "3"},  {4, "4"},  {5, "5"},  {6, "6"},  {7, "7"},  {8, "8"},  {9, "9"},
        {10, "A"}, {11, "B"}, {12, "C"}, {13, "D"}, {14, "E"}, {15, "F"}, {16, "G"}, {17, "H"}, {18, "I"}, {19, "J"},
        {20, "K"}, {21, "L"}, {22, "M"}, {23, "N"}, {24, "O"}, {25, "P"}, {26, "Q"}, {27, "R"}, {28, "S"}, {29, "T"},
        {30, "U"}, {31, "V"}, {32, "W"}, {33, "X"}, {34, "Y"}, {35, "Z"}, {36, "Ә"}, {37, "Б"}, {38, "Г"}, {39, "Д"},
        {40, "Ё"}, {41, "Ж"}, {42, "З"}, {43, "И"}, {44, "Й"}, {45, "Қ"}, {46, "Л"}, {47, "Ң"}, {48, "Ө"}, {49, "П"},
        {50, "У"}, {51, "Ұ"}, {52, "Ф"}, {53, "Ц"}, {54, "Ч"}, {55, "Ш"}, {56, "Щ"}, {57, "Ъ"}, {58, "Ы"}, {59, "Ь"},
        {60, "Э"}, {61, "Ю"}, {62, "Я"}, {63, "№"}, {64, "/"}, {65, "<"}, {66, "*"}, {67, ""}};

    std::map<int, std::string> mrz_classes_ = {
        {0, "0"},  {1, "1"},  {2, "2"},  {3, "3"},  {4, "4"},  {5, "5"},  {6, "6"},  {7, "7"},  {8, "8"},  {9, "9"},
        {10, "A"}, {11, "B"}, {12, "C"}, {13, "D"}, {14, "E"}, {15, "F"}, {16, "G"}, {17, "H"}, {18, "I"}, {19, "J"},
        {20, "K"}, {21, "L"}, {22, "M"}, {23, "N"}, {24, "O"}, {25, "P"}, {26, "Q"}, {27, "R"}, {28, "S"}, {29, "T"},
        {30, "U"}, {31, "V"}, {32, "W"}, {33, "X"}, {34, "Y"}, {35, "Z"}, {36, "<"}, {37, ""}};

    std::map<int, std::string> cyr_classes_ = {
        {0, "A"},  {1, "B"},  {2, "C"},  {3, "E"},  {4, "F"},  {5, "H"},  {6, "I"},  {7, "K"},  {8, "M"},
        {9, "O"},  {10, "P"}, {11, "T"}, {12, "X"}, {13, "Y"}, {14, "Ә"}, {15, "Б"}, {16, "Г"}, {17, "Д"},
        {18, "Ё"}, {19, "Ж"}, {20, "З"}, {21, "И"}, {22, "Й"}, {23, "Қ"}, {24, "Л"}, {25, "Ң"}, {26, "Ө"},
        {27, "П"}, {28, "У"}, {29, "Ұ"}, {30, "Ф"}, {31, "Ц"}, {32, "Ч"}, {33, "Ш"}, {34, "Щ"}, {35, "Ъ"},
        {36, "Ы"}, {37, "Ь"}, {38, "Э"}, {39, "Ю"}, {40, "Я"}, {41, ""}};

    std::map<int, std::string> digits_classes_ = {{0, "0"}, {1, "1"}, {2, "2"}, {3, "3"}, {4, "4"}, {5, "5"},
                                                  {6, "6"}, {7, "7"}, {8, "8"}, {9, "9"}, {10, ""}};

    // Maps to correct predictions
    std::map<std::string, std::string> latin_to_cyrr = {
        {"A", "А"}, {"B", "В"}, {"C", "С"}, {"D", "О"}, {"E", "Е"}, {"F", "Ғ"}, {"G", "С"}, {"H", "Н"}, {"I", "І"},
        {"J", "І"}, {"K", "К"}, {"L", "І"}, {"M", "М"}, {"N", "Н"}, {"O", "О"}, {"P", "Р"}, {"Q", "О"}, {"R", "Р"},
        {"S", "В"}, {"T", "Т"}, {"U", "О"}, {"V", "И"}, {"W", "Ш"}, {"X", "Х"}, {"Y", "У"}, {"Z", ""}};

    std::map<std::string, std::string> latin_to_digits = {
        {"A", ""},  {"B", "8"}, {"C", "0"}, {"D", "0"}, {"E", "6"}, {"F", ""},  {"G", "0"}, {"H", ""},  {"I", "1"},
        {"J", "1"}, {"K", ""},  {"L", "1"}, {"M", ""},  {"N", ""},  {"O", "0"}, {"P", "9"}, {"Q", "0"}, {"R", ""},
        {"S", "5"}, {"T", ""},  {"U", "0"}, {"V", ""},  {"W", ""},  {"X", ""},  {"Y", ""},  {"Z", "2"}};

    std::map<std::string, std::string> digits_to_cyrr = {{"0", "О"}, {"1", "І"}, {"2", ""},  {"3", "З"}, {"4", ""},
                                                         {"5", ""},  {"6", "Б"}, {"7", "Т"}, {"8", "В"}, {"9", ""}};
    std::map<std::string, std::string> digits_to_latin = {{"0", "O"}, {"1", "I"}, {"2", "Z"}, {"3", "B"}, {"4", ""},
                                                          {"5", "S"}, {"6", "B"}, {"7", "T"}, {"8", "B"}, {"9", ""}};

    std::map<std::string, std::string> cyrr_to_digits = {
        {"Ә", ""},  {"Б", "6"}, {"Г", ""}, {"Д", "0"}, {"Ё", ""}, {"Ж", ""},  {"З", "3"}, {"И", ""},  {"Й", ""},
        {"Қ", ""},  {"Л", ""},  {"Ң", ""}, {"Ө", "0"}, {"П", ""}, {"У", "9"}, {"Ұ", ""},  {"Ф", "0"}, {"Ц", ""},
        {"Ч", "4"}, {"Ш", ""},  {"Щ", ""}, {"Ъ", ""},  {"Ы", ""}, {"Ь", "6"}, {"Э", ""},  {"Ю", "0"}, {"Я", "9"}};

    std::map<std::string, std::string> cyrr_to_latin = {
        {"Ә", ""},  {"Б", "B"}, {"Г", "F"}, {"Д", "O"}, {"Ё", "E"}, {"Ж", ""},  {"З", "B"}, {"И", "N"}, {"Й", "N"},
        {"Қ", "K"}, {"Л", ""},  {"Ң", "H"}, {"Ө", "O"}, {"П", ""},  {"У", "Y"}, {"Ұ", "Y"}, {"Ф", "O"}, {"Ц", "U"},
        {"Ч", ""},  {"Ш", ""},  {"Щ", ""},  {"Ъ", "B"}, {"Ы", ""},  {"Ь", "B"}, {"Э", "B"}, {"Ю", "O"}, {"Я", ""}};

    std::map<std::string, std::string> cyrr_to_digits_and_latin = {
        {"Ә", "8"}, {"Б", "B"}, {"Г", "F"}, {"Д", "O"}, {"Ё", "E"}, {"Ж", ""},  {"З", "3"}, {"И", "N"}, {"Й", "N"},
        {"Қ", "K"}, {"Л", ""},  {"Ң", "H"}, {"Ө", "O"}, {"П", ""},  {"У", "Y"}, {"Ұ", "Y"}, {"Ф", "O"}, {"Ц", "U"},
        {"Ч", ""},  {"Ш", ""},  {"Щ", ""},  {"Ъ", "B"}, {"Ы", ""},  {"Ь", "B"}, {"Э", "B"}, {"Ю", "O"}, {"Я", ""}};

    std::map<std::string, std::string> mrzfiller_to_latin = {{"<", "K"}};

    std::map<std::string, std::string> symbols_to_veh_cert_id_number = {{"*", ""}, {"<", "K"}, {"/", ""}};
    std::map<std::string, std::string> symbols_to_latin = {{"№", "N"}, {"*", ""}, {"<", "K"}, {"/", ""}};
    std::map<std::string, std::string> symbols_to_digits = {{"№", "8"}, {"*", ""}, {"<", ""}, {"/", ""}};
    std::map<std::string, std::string> symbols_to_cyrr = {{"№", "Н"}, {"*", ""}, {"<", "К"}, {"/", ""}};
    std::map<std::string, std::string> symbols_to_mrz = {{"№", "N"}, {"*", ""}, {"/", ""}};
    std::map<std::string, std::string> symbols_to_cyrr_and_latin = {{"№", "N"}, {"*", ""}, {"<", "K"}};

  public:
    /**
     * Returns the size of the images for inputting into the classifier network.
     * @return
     */
    virtual cv::Size2i getInputSize();

    virtual std::string getInputBlobName();

    virtual std::string getOutputBlobName();

    /**
     * Returns the scale factor by which input data must be scaled
     * @return
     */
    virtual double getScaleFactor();

    //    void setModel(const cv::dnn::Net &net) override;

    void prepareData(const std::vector<cv::Mat> &chars) override;

    std::vector<std::pair<std::string, double>> predict(const int &field_type) override;

    virtual std::vector<std::pair<int, double>> getClasses(const cv::Mat &result);

    virtual std::vector<std::pair<std::string, double>> decode(const std::vector<std::pair<int, double>> &result,
                                                               const int &field_type);

    void correctPrediction(std::vector<std::pair<std::string, double>> &preds, const int &type) override;

    const std::map<int, std::string> &GetCharClasses() const;

    void correctConfidence(std::vector<std::pair<std::string, double>> &vector, const int &type) override;
};
} // namespace vdoc

#endif // VOCR_CLASSIFIER_OF_CHARS_H
