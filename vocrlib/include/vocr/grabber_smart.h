//
// Created by chingy on 8/1/18.
//

#ifndef VOCR_GRABBER_SMART_H
#define VOCR_GRABBER_SMART_H

#include "grabber_borders.h"
#include "grabber_fields.h"
#include <vocr/grabber.h>

namespace vdoc {
namespace grab {

class GrabberSmart : public Grabber {
  private:
  protected:
    void prepareVisualDocument() override;

  private:
    GrabberBorders grabber_borders_;
    GrabberFields grabber_fields_;

    // Maximum deviation coefficients from image boundaries
    float out_coeff_ = 0.03f;
    float inner_horiz_coeff_ = 0.07f;
    float inner_vert_coeff_ = 0.07f;

    bool grabber1();

    bool grabber2();

    float checkLayout(const cv::Mat &src, DocType &detected_type);

    /**
     * Indicates whether size of the found document result is large enough and
     * that it is not sticking out of the image boundaries too much.
     * @param result - grabbing result
     */
    bool checkRegion(std::shared_ptr<GrabbingResult> &result);

    /**
     * Checks if table is correctly located in Driving Licenses
     * @param res - grabbing result
     */
    bool checkTableFields(std::shared_ptr<GrabbingResult> &res);

  public:
    GrabberSmart() = default;

    GrabberSmart(DocType type) { setDocType(type); }

    bool process() override;

    bool isSame(std::shared_ptr<GrabbingResult> &res1, std::shared_ptr<GrabbingResult> &res2) override;

    bool grab(const cv::Mat &src, std::shared_ptr<GrabbingResult> &result) override;

    void setDocType(DocType type) override;

  protected:
    bool isSuccess() override;
};
} // namespace grab
} // namespace vdoc

#endif // VOCR_GRABBER_SMART_H
