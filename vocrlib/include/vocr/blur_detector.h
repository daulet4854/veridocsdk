#ifndef VOCR_BLUR_DETECTOR_H
#define VOCR_BLUR_DETECTOR_H

#include <map>
#include <vector>

#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <vocr/exception/blurry_image_exception.h>

#ifndef NDEBUG
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#endif

namespace vdoc {
namespace blur {

// Threshold for Blur value. If below -> image is too blurred. (meanStdDev)
const double blur_threshold_ = 9.0;

/**
 * Grabs a piece of image in the middle of the image, on
 * which we are going to detect blurriness. Resizes it to fit
 * it better in the blur detection procedure.
 */
cv::Mat prepareImage(const cv::Mat &image);

/**
 * Returns a value of how much the image is blurred (meanStdDev).
 * The lower the value, the blurrier image is.
 */
double blurValue(const cv::Mat &image);

/**
 * Returns an ordered vector of best unblurred images. First are better.
 * If image blur value is less than threshold -> it is excluded from the return vector.
 * If all images' blur values are less than threshold -> throws exception.
 */
std::vector<cv::Mat> topSharpImages(std::vector<cv::Mat> &images);

} // namespace blur
} // namespace vdoc

#endif // VOCR_BLUR_DETECTOR_H
