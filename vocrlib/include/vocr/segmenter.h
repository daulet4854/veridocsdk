//
// Created by chingy on 9/6/18.
//

#ifndef VOCR_SEGMENTER_BASIC_H
#define VOCR_SEGMENTER_BASIC_H

#include "../../src/visual_document/char.h"
#include "char_segmenter_cnn.h"
#include "interface.h"
#include "ocr_params.h"
#include "utils.h"
#include "utils_contours.h"
#include "utils_image.h"
#include <opencv2/core/mat.hpp>
#include <opencv2/opencv.hpp>
#include <stack>
#include <vector>

namespace vdoc {

namespace segmenter {

std::vector<cv::Mat> doBasic(const cv::Mat &word, const OcrParams &ocr, const int &field_type);

std::vector<cv::Mat> doBasicDigits(const cv::Mat &word, const OcrParams &ocr, const int &field_type, bool &is_success);

std::vector<cv::Mat> doRecursiveAdaThresh(const cv::Mat &word, const OcrParams &ocr, const int &field_type);

std::vector<cv::Mat> doCnn(const cv::Mat &word, const OcrParams &ocr, const int &field_type);

/**
 * Performs adaptive Gaussian threshold with constat 'c' taht substructed from calcualted threshold value
 * @param in
 * @param out
 * @param c
 */
static void adaptiveThreshold(const cv::Mat &in, cv::Mat &out, const int &c = 1);

/**
 * Finds and orders (from left to right) contours.
 * @param field Image of the field
 * @return Returns bounding rectangles of the contours
 */
static std::vector<cv::Rect> findCharContours(const cv::Mat &field);

/**
 * Tries to divide big segment of characters into individual characters
 * by tuning the thresholding parameters
 * @param img - word image
 * @param rect - bounding rectangle of characters
 * @return
 */
static std::vector<cv::Rect> separateChars(const cv::Mat &img, const cv::Rect &rect, const int &min_width,
                                           const int &min_height, const int &max_width);

/**
 * The function that determines whether provided rectangle is
 * look like a bounding box of some character (symbol)
 * @param rect
 * @return
 */
static bool isLikeCharacter(const cv::Rect &bbox);
} // namespace segmenter
} // namespace vdoc

#endif // VOCR_SEGMENTER_BASIC_H
