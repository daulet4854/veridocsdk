#ifndef DATE_FORMATTER_H
#define DATE_FORMATTER_H

#include <iostream>
#include <math.h>
#include <string>
#include <vector>

#include <vocr/stdx.h>

namespace vdoc {
class DateFormatter {
  public:
    bool assertSegmentLength(int value, int expected_value);
    bool assertSegmentRange(int value, int min, int max);
    std::string format(std::string input);
};
} // namespace vdoc

#endif // DATE_FORMATTER_H