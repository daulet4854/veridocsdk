//
// Created by chingy on 2/8/19.
//

#ifndef VOCR_CHAR_SEGMENTER_DIGITS_CNN_H
#define VOCR_CHAR_SEGMENTER_DIGITS_CNN_H

#include "char_segmenter_basic.h"
#include "char_segmenter_cnn.h"
#include "char_segmenter_cnn_models.h"

namespace vdoc {
class CharSegmenterDigitsCnn : public CharSegmenterCnn {
  public:
    CharSegmenterDigitsCnn() {
        setModel(std::make_shared<CharSegmenterDigitsCnnModel>());
        setThreshold(0.1);
        setMargins(2, 2);
    }

  protected:
    std::vector<float> processProbabilities(const std::vector<float> &src_probs) override;
};

class CharSegmenterDigits : public CharSegmenterInterface {
  public:
    CharSegmenterDigits() = default;

    explicit CharSegmenterDigits(const OcrParams &params) {
        basic_segmenter_ = std::make_shared<CharSegmenterDigitsBasic>(params);
        cnn_segmenter_ = std::make_shared<CharSegmenterDigitsCnn>();
    }

    std::vector<cv::Rect2i> segment(const cv::Mat &txt_line_img) override;

    cv::Size referenceSize() override;

    void setBasicSegmenter(const std::shared_ptr<CharSegmenterInterface> &basic_segmenter);

    void setCnnSegmenter(const std::shared_ptr<CharSegmenterInterface> &cnn_segmenter);

  private:
    std::shared_ptr<CharSegmenterInterface> basic_segmenter_;
    std::shared_ptr<CharSegmenterInterface> cnn_segmenter_;
};
} // namespace vdoc

#endif // VOCR_CHAR_SEGMENTER_DIGITS_CNN_H
