//
// Created by chingy on 7/31/18.
//

#ifndef VOCR_GRABBER_FIELDS_H
#define VOCR_GRABBER_FIELDS_H

#include "grabber.h"
#include "utils_contours.h"

namespace vdoc {
namespace grab {

/**
 * Point comparison for using in map structures
 */
struct PointComparator {
    bool operator()(const cv::Point2f &lhs, const cv::Point2f &rhs) const { return lhs.x < rhs.x; }
};

enum GrabberFieldsModes {
    GF_TEXTDET_EDGES = 1,
    GF_TEXTDET_ADATHRESH = 2,
    // Number 3 is reserved for both text detection methods
};

class GrabberFields : public Grabber {
  private:
  protected:
    void prepareVisualDocument() override;

  private:
    bool check_layout_ = false;

    /**
     * It is used to estimate contour position correctness value.
     * It will be doubled when used, because I consider max_deviation_distance_ is permissible emough.
     * So if deviation is equal to this maximum, the correctness value will be 0.5
     */
    int max_deviation_distance_ = 8;

    /**
     * Since document image is usually transformed somehow, contours are also transformed.
     * So we consider some possible deviations from ideal document layout
     */
    float absolute_deviation_ = 0.3;

    /**
     * maximum tolerable metrical deviation ratio
     */
    float max_deviation_ratio_;
    /**
     * minimum tolerable metrical deviation ratio
     */
    float min_deviation_ratio_;

    /**
     * maximum absolute angle difference between parallel lines
     */
    float max_angle_diff_ = 30;

    const cv::Mat kernel_for_opening_ = cv::getStructuringElement(cv::MORPH_RECT, {3, 3});
    const std::vector<cv::Mat> kernel_for_closeop_ = {cv::getStructuringElement(cv::MORPH_RECT, {8, 3}),
                                                      cv::getStructuringElement(cv::MORPH_RECT, {16, 3})};

    /**
     * Threshold for confidence score
     * @return
     */
    float threshConfidenceScore();

    bool isDocTypeNameInCollection(const std::string &doc_type_name, const std::vector<DocType> &types);

    /**
     * For now text contours can be found by one of two methods:
     * GF_TEXTDET_EDGES or GF_TEXTDET_ADATHRESH
     * @param src
     * @param method
     * @return
     */
    vdoc::ContoursList findTextContours(const cv::Mat &src, const int &method);

    /**
     * Handles contour analysis and finds document corners in source image
     * Returns confidence_rate of detection. Its value is between 0 and 1.
     * Detection results are saved in input parameters vertices, doc_type and ar (aspect ratio).
     * If doc_type value other than UNKNOWN is passed, then only passed document template is searched.
     * Use "checkGrabbedImage" only if you need find doc_type and confidence rate for already grabbed image
     * @param contours
     * @param doc_type
     * @param vertices
     * @param ar
     * @param checkGrabbedImage
     * @return
     */
    float analyseContours(const vdoc::ContoursList &contours, DocType &doc_type, std::vector<cv::Point2f> &vertices,
                          float &ar, bool checkGrabbedImage = false);

    /**
     * Finds RotatedRects of contours and filters them by angle, height and aspect ratio
     * @param contours
     * @param filtered_rects
     */
    void filterContourRotRects(const vdoc::ContoursList &contours, std::vector<cv::RotatedRect> &filtered_rects);

    /**
     * Finds possible additional or child contours for further analysis and stores in a map.
     * For each "child or additional contour template in the JSON arrays" we store
     * "vector of RotatedRects of all possible contours that fit this contour template".
     * map keys - indexes of each child or additional contour template in the JSON array.
     * map values - vector of RotatedRects of all possible contours that fit this contour template.
     * @param curr_template
     * @param contour_type
     * @param base_rr
     * @param contours_by_tl_x_axis
     * @param contours_by_tr_x_axis
     * @param contours_by_x_axis
     * @return
     */
    std::map<int, std::vector<cv::RotatedRect *>>
    findFittingChildContours(nlohmann::json &curr_template, const std::string &contour_type,
                             const cv::RotatedRect &base_rr,
                             std::multimap<cv::Point2f, cv::RotatedRect *, PointComparator> &contours_by_tl_x_axis,
                             std::multimap<cv::Point2f, cv::RotatedRect *, PointComparator> &contours_by_tr_x_axis,
                             std::multimap<cv::Point2f, cv::RotatedRect *, PointComparator> &contours_by_x_axis);

    /**
     * Transforms RotatedRect positions of given possible template fitting contours
     * perspectively and updates passed confidence rate
     * @param curr_template
     * @param base_rr_copy
     * @param curr_child_index
     * @param all_contours_count
     * @param possible_contours
     * @param contour_type
     * @param transform_mat
     * @param confidence_rate
     */
    void checkTransformedContours(nlohmann::json &curr_template, cv::RotatedRect &base_rr_copy, int curr_child_index,
                                  int all_contours_count,
                                  std::map<int, std::vector<cv::RotatedRect *>> possible_contours,
                                  const std::string &contour_type, cv::Mat transform_mat, float &confidence_rate);

    /**
     * Saves coordinates of four bounding points for two contours into bound_pts
     * Two upper corners of upper rectangle and two lower corners of lower rectangle are taken
     * @param first_rect
     * @param second_rect
     * @param bound_pts
     * @param y_gap
     */
    void getBoundingPoints(const cv::RotatedRect &first_rect, const cv::RotatedRect &second_rect,
                           cv::Point2f *bound_pts, float y_gap);

    /**
     * This function finds perspective transformation matrix that skewed document image.
     * It uses four bounding points for considered base and child contours (estimated by getBoundingPoints).
     * If is_correction flag is true it finds deskew transformation matrix. It finds skew matrix otherwise.
     * @param base_rr
     * @param child_rr
     * @param child_contour_template
     * @param base_aspect_ratio
     * @param is_correction
     * @return
     */
    cv::Mat findTransformationFromRects(cv::RotatedRect &base_rr, cv::RotatedRect &child_rr,
                                        nlohmann::json &child_contour_template, float base_aspect_ratio,
                                        bool is_correction);

    /**
     * For given RotatedRect object, it returns needed corner point using passed parameters
     * @param rot_rect
     * @param contour_type
     * @param check_point
     * @return
     */
    cv::Point2f getCheckPoint(cv::RotatedRect &rot_rect, const std::string &contour_type,
                              const std::string &check_point);

  public:
    GrabberFields() {
        // Default text detection is ADAPTIVE THRESHOLDING
        setMode(GF_TEXTDET_ADATHRESH);

        min_deviation_ratio_ = 1 - absolute_deviation_;
        max_deviation_ratio_ = 1 + absolute_deviation_;
    }

    bool grab(const cv::Mat &src, std::shared_ptr<GrabbingResult> &result) override;

    bool process() override;

    bool isSame(std::shared_ptr<GrabbingResult> &res1, std::shared_ptr<GrabbingResult> &res2) override;

    /**
     * Checks fitting of the given grabbed document image to the passed doc_type,
     * or if doc_type is UNKNOWN all document types are checked.
     * Saves found doc_type in passed parameter, and returns confidence rate of fitting.
     * @param src
     * @param doc_type
     * @return
     */
    float checkLayout(const cv::Mat &src, vdoc::DocType &doc_type);

  protected:
    bool isSuccess() override;

    int getProcessingWidth() override;
};
} // namespace grab
} // namespace vdoc

#endif // VOCR_GRABBER_FIELDS_H
