#ifndef C_API_WRAPPER_H
#define C_API_WRAPPER_H

#include <cstring>
#include <mutex>
#include <string>
#include <vector>
#include <vocr/video_adapter.h>
#include <vocr/vocr.h>

/**
 * A map of VideoAdapters for each user, mapping user session keys to according adapter and
 * its last update time
 */
static std::map<std::string, std::pair<vdoc::VideoAdapter, std::chrono::high_resolution_clock::time_point>>
    video_adapters;

/**
 * Mutex for blocking video_adapters map when doing not thread-safe calls
 */
static std::mutex adapters_map_mutex;

/**
 * @brief Checks if VideoAdapter for this user already exists or if it needs to be updated
 *
 * @param user_id user id from django user object
 * @param doc_type document type of this image according to DocType enum
 */
static void CheckVideoAdapter(const std::string &user_id, int doc_type);

/**
 * Checks video_adapters map for containing only active users.
 * If non-active users are found, they are removed.
 */
static void UpdateAdapters(std::chrono::high_resolution_clock::time_point right_now);

extern "C" {

/**
 * @brief Determines whether there is a document in image file
 *
 * @param image_path path to image file
 * @param image_id index of image sent from single user
 * @param doc_type document type of this image according to DocType enum
 * @param user_id user id from django user object
 * @param result_buffer Buffer for results
 */
void NextImage(const char *image_path, int image_id, int doc_type, const char *user_id, char *result_buffer);

/**
 * @brief Sets original image with the best image_id to VideoAdapter and does recognition
 *
 * @param image_path path to original image file
 * @param image_id index of current best image
 * @param doc_type document type of this image according to DocType enum
 * @param user_id user id from django user object
 * @param result_buffer Buffer for results
 */
void SetImage(const char *image_path, int image_id, int doc_type, const char *user_id, char *result_buffer);

/**
 * @brief Grabs document in the image and returns grabbing result
 *        Warning: For Quality Test purposes only!!!
 *
 * @param image_path path to original image file
 * @param doc_type document type of this image according to DocType enum
 * @param archetype needed archetype integer according to Archetype enum
 * @param result_buffer Buffer for results
 */
void GrabDocument(const char *image_path, int doc_type, char *result_buffer);

// TODO: OPTIMIZATION instead of passing file paths, send byte array of file content and
//                    read image directly from Python memory (not from file system)
/**
 * C API for image recognition - one image
 *
 * @param image_path Path to image file (jpg, png, etc.)
 * @param doc_type Type of the document be recognized (same as DocType enum, but
 * an int) (0 for UNKNOWN)
 * @param extract_images whether to extract image fields as well, or only text fields
 * @param result_buffer Buffer for results (Note: should be large enough)
 */
void Extract(const char *image_path, int doc_type, bool extract_images, char *result_buffer);

void ExtractWithoutGrabber(const char *image_path, int doc_type, bool extract_images, char *result_buffer);
/**
 * C API for image recognition - several images
 *
 * @param image_path_array Pointer to array of C strings of file paths to images
 * @param image_qty Size of image_path_array
 * @param doc_type Type of the document to be recognized (same as DocType enum, but
 * an int) (0 for UNKNOWN)
 * @param extract_images whether to extract image fields as well, or only text fields
 * @param result_buffer Buffer for results (Note: should be large enough)
 */
void SequenceExtract(const char **image_path_array, int image_qty, int doc_type, bool extract_images,
                     char *result_buffer);

/**
 * @brief Extract MRZ from a single image file
 *
 * @param image_path path to image file
 * @param result_buffer Buffer for results (Note: should be large enough)
 */
void ExtractMRZ(const char *image_path, char *result_buffer);

/**
 * @brief C API for MRZ recognition - several images (stream)
 *
 * @param image_path_array Pointer to array of C strings of file paths to images
 * @param image_qty Size of image_path_array
 * @param result_buffer Buffer for results (Note: should be large enough)
 */
void SequenceExtractMRZ(const char **image_path_array, int image_qty, char *result_buffer);

/**
 * @brief Determines whether there is an MRZ region in image file.
 * Used mainly for autocapturing MRZ in the WEB version.
 *
 * @param image_path path to image file
 * @param result_buffer Buffer for results - JSON string format:
 *         {
 *           "found": true, // If found MRZ in the img
 *           "lines": [ // List of coordinates of mrz lines (max 3 lines)
 *                     { // Coordinates of one line
 *                       "tl": {"x": 56, "y": 227},   // Top-left
 *                       "tr": {"x": 446, "y": 225},  // Top-right
 *                       "br": {"x": 56, "y": 211},   // Bottom-right
 *                       "bl": {"x": 445, "y": 209}   // Bottom-left
 *                     },
 *                     ...
 *                    ]
 *         }
 */
void NextImageMRZ(const char *image_path, char *result_buffer);

/**
 * @brief Predicts whether given image is a corner
 *
 * @param img Image to predict
 * @return int 0 - no corner, 1 - corner
 */
int isCorner(const char *corner_image_path);

/**
 * Mainly for quality test
 */
// int PredictDocType(const char *image_path);
int PredictMRZType(const char *image_path);
}

#endif
