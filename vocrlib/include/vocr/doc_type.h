#ifndef VOCR_DOC_TYPE_H
#define VOCR_DOC_TYPE_H

#include <string>
#include <vector>

namespace vdoc {

/**
 * [  0  ] * UNKNOWN ************************** Assumes, that the type of the document is deducted by type classifier
 * [1 - 2] * KZ_ID_1_[FRONT|BACK] ************* An old ID without IIN and Bar Code
 * [3 - 4] * KZ_ID_2_[FRONT|BACK] ************* An old ID with IIN and Bar Code at the back
 * [5 - 6] * KZ_ID_3_[FRONT|BACK] ************* An old ID standard (valid until December 1, 2020) with a chip
 * [7 - 8] * KZ_ID_4_[FRONT|BACK] ************* The latest ID standard with (coloured photo & golographic image)
 * [  9  ] * KZ_PASSPORT_MAIN ***************** A passport
 * [ 10  ] * KZ_DRIVING_LICENCE_1 ************* A driver licence (an old standard)
 * [ 11  ] * KZ_DRIVING_LICENCE_2 ************* A driver licence (since March 3, 2015)
 * [12-13] * KZ_VEHICLEREGCERT_1_[FRONT|BACK] * A vehicle registration certificate OLD type
 * [14-15] * KZ_VEHICLEREGCERT_2_[FRONT|BACK] * A vehicle registration certificate NEW type
 * [ 16 ]  * MRZ ******************************
 * [ 17 ]  * VEON_SIM_CARD ********************

 **/
enum class DocType {
    UNKNOWN,                   // 0
    KZ_ID,                     // 1
    KZ_ID_FRONT,               // 2
    KZ_ID_BACK,                // 3
    KZ_ID_1,                   // 4
    KZ_ID_1_FRONT,             // 5
    KZ_ID_1_BACK,              // 6
    KZ_ID_2,                   // 7
    KZ_ID_2_FRONT,             // 8
    KZ_ID_2_BACK,              // 9
    KZ_ID_3,                   // 10
    KZ_ID_3_FRONT,             // 11
    KZ_ID_3_BACK,              // 12
    KZ_ID_4,                   // 13
    KZ_ID_4_FRONT,             // 14
    KZ_ID_4_BACK,              // 15
    KZ_PASSPORT,               // 16
    KZ_DRIVER_LICENCE,         // 17
    KZ_DRIVER_LICENCE_1,       // 18
    KZ_DRIVER_LICENCE_2,       // 19
    KZ_VEHICLEREGCERT,         // 20
    KZ_VEHICLEREGCERT_1,       // 21
    KZ_VEHICLEREGCERT_1_FRONT, // 22
    KZ_VEHICLEREGCERT_1_BACK,  // 23
    KZ_VEHICLEREGCERT_2,       // 24
    KZ_VEHICLEREGCERT_2_FRONT, // 25
    KZ_VEHICLEREGCERT_2_BACK,  // 26
    KZ_VEHICLEREGCERT_3,       // 27
    MRZ,                       // 28
    MRZ_TD1,                   // 29
    MRZ_TD2,                   // 30
    MRZ_TD3,                   // 31
    BEELINE_SIMCARD            // 32
};

std::vector<vdoc::DocType> getLeaves(const DocType &doc_type);

std::string toString(const DocType &doc_type);

vdoc::DocType docTypeByName(const std::string &doc_type_name);

int docTypeId(DocType doc_type);

} // namespace vdoc

#endif // VOCR_DOC_TYPE_H
