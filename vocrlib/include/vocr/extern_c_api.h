#ifndef EXTERN_C_API_H
#define EXTERN_C_API_H

#include <vocr/extern_c_native.h>

#ifdef __linux__
extern "C" int extern_c_calc_rect_area(int a, int b);
extern "C" bool extern_c_detect(int cv_type, unsigned char *buffer, int cols, int rows);
extern "C" void extern_c_extract(char *buffer);
extern "C" void extern_c_set_document_type(int ordinal);
#elif _WIN32
extern "C" __declspec(dllexport) int extern_c_calc_rect_area(int a, int b);
extern "C" __declspec(dllexport) bool extern_c_detect(int cv_type, unsigned char *buffer, int cols, int rows);
extern "C" __declspec(dllexport) void extern_c_extract(char *buffer);
extern "C" __declspec(dllexport) void extern_c_set_document_type(int ordinal);
#elif __APPLE__
extern "C" int extern_c_calc_rect_area(int a, int b);
extern "C" bool extern_c_detect(int cv_type, unsigned char *buffer, int cols, int rows);
extern "C" void extern_c_extract(char *buffer);
extern "C" void extern_c_set_document_type(int ordinal);
#endif

#endif // EXTERN_C_API_H