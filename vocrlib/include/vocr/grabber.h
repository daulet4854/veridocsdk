//
// Created by chingy on 7/24/18.
//
#ifndef VOCR_GRABBER_H
#define VOCR_GRABBER_H

#include "vocr/doc_type.h"
#include <opencv2/core/mat.hpp>
#include <vocr/vocr.h>

namespace vdoc {

namespace grab {

/**
 * Returns the aspect ratio of a docuemnt depending on its type and archetype
 * @param type
 * @param atype
 * @return
 */
float aspectRatio(DocType &type);

/**
 * Finds the areas with text. Returns the binary mask of the original image.
 * @param src
 * @param dst
 * @param kernel
 */
void detectText(const cv::Mat &src, cv::Mat &dst, const cv::Mat &kernel, bool is_possibly_white = false);

struct Line {
    short label = -1;
    cv::Point2i p1;
    cv::Point2i p2;
};

struct Region {
  public:
    std::string label = "";
    std::vector<cv::Point2f> keypoints;

    Region(std::string _label, std::vector<cv::Point2f> _keypoints)
        : label(std::move(_label)), keypoints(std::move(_keypoints)) {}

    Region() = default;
};

struct GrabbingResult {
    float confidence_score = 0.0f;
    std::vector<Region> regions;
    std::vector<Line> lines;
    std::vector<cv::Mat> images;
    DocType type = DocType::UNKNOWN;
    std::shared_ptr<VisualDocument> visdoc;
    bool text_detected = false;

    bool empty() { return regions.empty() && lines.empty() && images.empty(); }
};

class Grabber {

  private:
    bool video_mode_ = false;

  protected:
    int mode_ = -1;
    float scale_ = 1.0f;
    DocType type_ = DocType::UNKNOWN;
    //    Archetype atype_ = Archetype::ARCH_UNKNOWN;
    cv::Mat img_original_;
    cv::Mat img_original_gray_;
    cv::Mat img_scaled_;
    std::shared_ptr<GrabbingResult> result_ = std::make_shared<GrabbingResult>();

    /**
     * Returns the width of the image which will be used for processing. Derived classes can override the method ,
     * and return different value. As a result all processings will be performed with corresponding resolution.
     * @return Default 500.
     */
    virtual int getProcessingWidth();

    /**
     * Interpretation of the results. Since GrabbingResult objects have many fields it is not obvious
     * whether acquired values should be considered as a success. Each type of grabber should implement it is own
     * interpretation of the results
     * @return
     */
    virtual bool isSuccess() = 0;

    /**
     * Adds the keypoints to the regions in the GrabbingResult object
     */
    virtual void addRegion2Result(const std::vector<cv::Point2f> &keypoints);

    /**
     * Creates a VisualDocument and assigns it to the GrabbingResult.
     * It is invocated when all the values are in the original scale.
     */
    virtual void prepareVisualDocument() = 0;

  public:
    /**
     * Main processing of the image is performed here.
     * @return
     */
    virtual bool process() = 0;

    /**
     * Convenience function that calls setImage(), scaleInputImage(), process(), rescaleResult() and warp()
     * functions sequentially.
     * @param src
     * @param result
     * @return
     */
    virtual bool grab(const cv::Mat &src, std::shared_ptr<GrabbingResult> &result);

    virtual bool grab(const cv::Mat &src, std::shared_ptr<GrabbingResult> &result, DocType type);

    virtual bool isSame(std::shared_ptr<GrabbingResult> &res1, std::shared_ptr<GrabbingResult> &res2) = 0;

    void warp();

    std::shared_ptr<GrabbingResult> getResult();

    void setMode(const int &mode);

    void setVideoMode(bool video_mode);

    bool isVideoMode();

    virtual void setDocType(DocType type);

    void setImage(const cv::Mat &img_original);

    void setImage(const cv::Mat &img_original, DocType type);

    void scaleInputImage();

    void rescaleResult();

    static std::shared_ptr<Grabber> make_grabber(const DocType &type);
};
} // namespace grab
} // namespace vdoc

#endif // VOCR_GRABBER_H
