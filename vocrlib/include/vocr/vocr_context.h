//
// Created by chingy on 2/16/19.
//

#ifndef VOCR_VOCRCONTEXT_H
#define VOCR_VOCRCONTEXT_H

#include "doc_type.h"

namespace vdoc {
struct VocrContext {

    DocType doc_type = DocType::UNKNOWN;
    std::string field_name;
    int char_type = 0;
};
} // namespace vdoc
#endif // VOCR_VOCRCONTEXT_H
