//
// Created by chingy on 2/11/19.
//

#ifndef VOCR_CHAR_SEGMENTER_HYBRIDSLASH_CNN_H
#define VOCR_CHAR_SEGMENTER_HYBRIDSLASH_CNN_H

#include "char_segmenter_interface.h"
#include "dnn_model_interface.h"
#include <memory>

namespace vdoc {

class CharSegmenterHybrid : public CharSegmenterInterface {

  public:
    std::vector<cv::Rect2i> segment(const cv::Mat &txt_line_img) override;

    cv::Size referenceSize() override;

    void setDividerSegmenter(const std::shared_ptr<CharSegmenterInterface> &segmenter);
    void setLeftSideSegmenter(const std::shared_ptr<CharSegmenterInterface> &segmenter);
    void setRightPartSegmenter(const std::shared_ptr<CharSegmenterInterface> &segmenter);

    void setDividerPadding(float pad1, float pad2);

  private:
    cv::Mat cropForDivider(const cv::Mat &src, float padding);
    cv::Rect2i findDivider(const cv::Mat &src);
    cv::Rect2i cropAndFindDivider(const cv::Mat &src);
    std::vector<cv::Rect2i> cropAndSegmentLeftSide(const cv::Mat &src, const cv::Rect2i &div) const;
    cv::Mat cropLeftSide(const cv::Mat &src, const cv::Rect2i &divider) const;
    void correctDividerPositionRelativeToWholeInputImage(cv::Rect2i &div);

    std::shared_ptr<CharSegmenterInterface> divider_segmenter_;
    std::shared_ptr<CharSegmenterInterface> left_segmenter_;
    std::shared_ptr<CharSegmenterInterface> right_segmenter_;

    float right_pad_1_ = 0.1f;
    float right_pad_2_ = 0.4f;
    int translation_x_ = 0;
};
} // namespace vdoc

#endif // VOCR_CHAR_SEGMENTER_HYBRIDSLASH_CNN_H
