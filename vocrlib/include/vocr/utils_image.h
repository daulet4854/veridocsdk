//
// Created by Chingiz Kenshimov on 1/22/19.
//

#ifndef VOCR_UTILS_IMAGE_H
#define VOCR_UTILS_IMAGE_H

#include <opencv2/imgproc.hpp>

namespace vdoc {
namespace imutils {

/**
 * Temporary variable to store height scaling value between function calls
 */
static float height_resizing_scale = 1.0f;

cv::Mat resizeHeightKeepingAspectRatio(const cv::Mat &input, int height);

cv::Mat resizeHeightKeepingAspectRatio(const cv::Mat &input, int height, float &scale);

cv::Mat shrinkWidth(const cv::Mat &input, int width);

cv::Mat zeroPadAlongWidth(const cv::Mat &input, int width);

cv::Mat toGray(const cv::Mat &color);

/**
 * If provided Rect dimensions exceeds image bounds, then this procedure assigns most extreme coordinates possible
 * @param roi
 * @param img_width
 * @param img_height
 */
cv::Rect2i correctRoi(const cv::Rect2i &roi, const cv::Size &image_size);

/**
 * Safely crops the provided image using the ROI (Region of interest) rectangle. If ROI exceeds bounds of the image
 * it crops using the maximum allowed coordinates by the input image.
 * @param src
 * @param roi
 * @param dst
 */
cv::Mat safeCrop(const cv::Mat &img, const cv::Rect2i &roi);

/**
 * Safely crops the provided image using the ROI (Region of interest) rectangles. If ROI exceeds bounds of the image
 * it crops using the maximum allowed coordinates by the input image.
 * @param src
 * @param roi
 * @param dst
 */
std::vector<cv::Mat> safeCrop(const cv::Mat &img, const std::vector<cv::Rect2i> &rois);

} // namespace imutils
} // namespace vdoc

#endif // VOCR_UTILS_IMAGE_H
