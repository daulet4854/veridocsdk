#ifndef VOCR_UTILS_ID_H
#define VOCR_UTILS_ID_H

#include <vocr/utils.h>
#include <vocr/utils_char.h>
#include <vocr/utils_mrz.h>
#include <vocr/visual_document.h>

namespace vdoc {
namespace id {

bool CheckFrontSideFields(std::shared_ptr<vdoc::VisualDocument> vis_doc);

bool CheckBackSideFields(std::shared_ptr<vdoc::VisualDocument> vis_doc);

bool CheckTwoSideConformity(std::shared_ptr<vdoc::VisualDocument> front_vis_doc,
                            std::shared_ptr<vdoc::VisualDocument> back_vis_doc);

} // namespace id
} // namespace vdoc

#endif
