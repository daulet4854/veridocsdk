//
// Created by chingy on 11/21/17.
//

#ifndef VOCR_CVPARAMS_H
#define VOCR_CVPARAMS_H

#include <opencv2/core.hpp>

namespace vdoc {
struct CVParams {

    cv::Size kernel_size;
    int sigma_x;
};
} // namespace vdoc

#endif // VOCR_CVPARAMS_H
