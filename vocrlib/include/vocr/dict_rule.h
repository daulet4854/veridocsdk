#ifndef VOCR_DICT_RULE_H
#define VOCR_DICT_RULE_H

#include <string>
#include <vector>

#include <vocr/rule_interface.h>

namespace vdoc {

/**
 * Base class of all dictionary replacement rules based on field_name only.
 */
class DictRule : public RuleInterface {
  private:
    /**
     * Dictionary of possible words that should be in the field with name `field_name_`
     */
    std::vector<std::string> dict_;

    /**
     * Word is replaced form `dict_` iff edit distance is lower than original_word.length() * threshold_
     */
    double threshold_;

  public:
    /**
     * Helper function: given two strings and their length, finds minimal edit distance between two words.
     * Edits include: replace char, insert char, delete char (Levenshtein Distance, dynamic programming)
     * TODO: Convert to UTF-8 strings!
     * @param str1 - first string
     * @param str1 - second string
     * @param m - length of str1
     * @param n - length of str2
     * @return - edit distance
     */
    int wordEditDistance(const std::string &str1, const std::string &str2, int m, int n);

    /**
     * Finds dictionary replacement with the closest word (only certain text fields)
     *
     * @param original_word given word to find close in dictionary
     * @return std::string replaced word. If no replacement, it returns 'original_word'
     */
    std::string replaceFromDictionary(const std::string &original_word);

    /**
     * Name of the field for which to do dict. replacement
     */
    std::string field_name_;

    /**
     * Construct a new Dict Rule object
     *
     * @param field_name Name of the field for which do dict. replacement
     * @param dict Dictionary of possible words that should be in the field
     * @param threshold Threshold for dict. replacement (see `threshold_`)
     */
    DictRule(std::string field_name, std::vector<std::string> dict, double threshold = 0.4);

    void apply(std::shared_ptr<vdoc::VisualDocument> &vis_doc) override;
};
} // namespace vdoc

#endif // VOCR_DICT_RULE_H
