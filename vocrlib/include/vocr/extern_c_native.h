#ifndef EXTERN_C_NATIVE_H
#define EXTERN_C_NATIVE_H

#include <vocr/video_adapter.h>

/**
 * A static instance of vdoc::VideoAdapter that is responsible for document recognition and text extraction
 */
static auto vdoc_video_adapter = std::make_shared<vdoc::VideoAdapter>(vdoc::DocType::UNKNOWN, 0);

#endif // EXTERN_C_NATIVE_H