//
// Created by chingy on 8/15/18.
//

#ifndef VOCR_GRABBER_BEELINESIMCARD_H
#define VOCR_GRABBER_BEELINESIMCARD_H

#include "grabber.h"

namespace vdoc {
namespace grab {

class GrabberBeelineSimCard : public Grabber {

    const float min_ar_ = 0.05;
    const float max_ar_ = 0.12;

    void addMargins(std::vector<cv::Point2f> &corners, const double &angle);

    std::shared_ptr<vdoc::VisualDocument> createVisualDocument(cv::Mat &img);

    // determines whether image is of the B2B sim-card
    bool isDark(const cv::Mat &src);

  public:
    GrabberBeelineSimCard() { type_ = DocType::BEELINE_SIMCARD; }

    /**
     * Doctype for this grabber is BEELINE_SIMCARD.
     * It is overriden to ignore the setting other Doctype from the outside.
     * @param type
     * @param archetype
     */
    void setDocType(DocType type) override;

    bool process() override;

    bool isSame(std::shared_ptr<GrabbingResult> &res1, std::shared_ptr<GrabbingResult> &res2) override;

  protected:
    bool isSuccess() override;

    void prepareVisualDocument() override;
};
} // namespace grab
} // namespace vdoc

#endif // VOCR_GRABBER_BEELINESIMCARD_H
