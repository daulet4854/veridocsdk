#ifndef VOCR_UTILS_RULE_H
#define VOCR_UTILS_RULE_H

#include "visual_document.h"
#include <regex>
#include <vocr/utils_char.h>

namespace vdoc {
namespace util {
namespace rules {

/**
 * Finds cyrillic part of string using {getCyrillicPart} method and sets the resulting string
 * to the field using its passed name.
 * @param field_name name of the field
 * @return
 */
void setCyrillicPartToField(std::string field_name, std::shared_ptr<vdoc::VisualDocument> &vis_doc);

/**
 * Finds cyrillic part in [Cyrillic/Latin] formatted string, replaces Latin letters in it with
 * similar looking Cyrillic letters
 * @param field_value string value of the field
 * @return string value of only Cyrillic part of the field
 */
std::string getCyrillicPart(std::string field_value);

/**
 * Tries to correct year using {tryFittingYearBetween1950And2050} and checks if this is a correct date
 * in the DDMMYYYY format without any delimiters.
 * @param string value of date (modified by function)
 * @return true if corrected value is a possible date, and false otherwise
 */
bool tryToCorrectDate(std::string &date_str);

/**
 * Tries to fit the value of year to the year interval from 1950 to 2050 if possible.
 * @param string value of year (modified by function)
 * @return true if corrected value lies in the given interval, and false otherwise
 */
bool tryFittingYearBetween1950And2050(std::string &year_str);

/**
 * Finds two parts in the string and saves them in two other passed strings if $ symbol divider is found
 * @param text string with two substrings concatenated by $ symbol.
 * @param first_part string for storing left substring of value
 * @param second_part string for storing right substring of value
 * @return true if division is successful, false otherwise
 */
bool divideByDollarSign(std::string text, std::string &first_part, std::string &second_part);

} // namespace rules
} // namespace util
} // namespace vdoc
#endif