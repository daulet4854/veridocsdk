#ifndef VOCR_UTILS_CHAR_H
#define VOCR_UTILS_CHAR_H

#include <map>
#include <set>
#include <string>

namespace vdoc {
namespace util {
namespace chars {

/**
 * Map of all Latin characters to similar looking Digit characters.
 */
extern std::map<char, char> latin_to_digits;

/**
 * Map of all Digit characters to similar looking Latin characters.
 */
extern std::map<char, char> digits_to_latin;

/**
 * Map of all Kazakh-only characters to similar looking Russian characters.
 */
extern std::map<wchar_t, wchar_t> kazakh_to_russian;

/**
 * Map of all Latin characters to similar looking Cyrillic characters.
 */
extern std::map<std::string, std::string> latin_to_cyrillic;

/**
 * Map of all Cyrillic characters transliterated in Latin.
 * TODO: check if this transliteration is the most common variant
 */
extern std::map<std::string, std::string> cyrr_transliterated;

/**
 * Map of all Cyrillic characters transliterated in Latin used for MRZ zone in Kazakhstan IDs
 * TODO: check if this transliteration has all transliteration rules
 */
extern std::map<std::string, std::string> mrz_cyrr_transliterated;

/**
 * Map of all Cyrillic vowel characters
 */
extern std::set<std::string> cyrr_vowel_letters;

/**
 * Returns a string that have all characters replaced with a corresponding character in char_map. Replaces characters
 * only if mapping exist in char_map.
 *
 * @param input input string
 * @param char_map map of characters to characters to be replaced
 * @return std::string new replaced string
 */
std::string replace(std::string input, std::map<char, char> char_map);

/**
 * Returns a string that have all characters replaced with a corresponding character in char_map. Replaces characters
 * only if mapping exist in char_map.
 *
 * @param input input string
 * @param char_map map of characters to characters to be replaced
 * @return std::wstring new replaced string
 */
std::wstring replace(std::wstring input, std::map<wchar_t, wchar_t> char_map);

/**
 * Returns a string that have all substrings replaced with a corresponding substring in str_map. Replaces substrings
 * only if mapping exist in str_map.
 *
 * @param input input string
 * @param str_map map of strings to strings to be replaced
 * @return std::string new replaced string
 */
std::string replace(std::string input, std::map<std::string, std::string> str_map);

/**
 * Returns a string that have all characters replaced with ch.
 *
 * @param input input string
 * @param ch character to replace with
 * @return std::string
 */
std::string replace(std::string input, char ch);

/**
 * Returns a string that have all Latin characters replaced to Digit characters.
 *
 * @param input input string
 * @return std::string new replaced string
 */
std::string ltod(std::string input);

/**
 * Replaces all Digit characters to Latin characters in a string (not in-place)
 *
 * @param input input string
 * @return std::string new replaced string
 */
std::string dtol(std::string input);

/**
 * Replaces all Kazakh characters to Russian characters in a string (not in-place)
 *
 * @param input input string
 * @return std::wstring new replaced string
 */
std::wstring ktor(std::wstring input);

/**
 * Replaces all Latin characters to cyrillic characters in a string (not in-place)
 *
 * @param input input string
 * @return std::string new replaced string
 */
std::string ltoc(std::string input);

/**
 * Transliterates all Cyrillic characters to Latin characters in a string
 *
 * @param input input string
 * @return std::string new transliterated string
 */
std::string toLatinTransliterated(std::string input);

/**
 * Transliterates all Cyrillic characters to Latin characters in a string using MRZ rules
 */
std::string toMrzLatinTransliterated(std::string cyrillic_word);
} // namespace chars
} // namespace util
} // namespace vdoc
#endif
