//
// Created by chingy on 2/16/19.
//

#ifndef VOCR_IMAGE_PROC_INTERFACE_H
#define VOCR_IMAGE_PROC_INTERFACE_H

#include <opencv2/core/mat.hpp>

namespace vdoc {
class ImageProcessorInterface {
  public:
    virtual void process(const cv::Mat &src) = 0;
    virtual cv::Mat processed() = 0;
    virtual std::vector<cv::Rect> rois() = 0;
    virtual std::vector<cv::RotatedRect> fineRois() = 0;

    virtual ~ImageProcessorInterface() = default;
};
} // namespace vdoc

#endif // VOCR_IMAGE_PROC_INTERFACE_H
