//
// Created by chingy on 10/17/17.
//

#ifndef VOCR_WORDINATORSIMPLE_H
#define VOCR_WORDINATORSIMPLE_H

#include "char_segmenter_interface.h"
#include "wordinator_interface.h"
#include <memory>

namespace vdoc {

class VocrWordinator : public vdoc::VocrWordinatorInterface {
  public:
    void segmentChars(std::shared_ptr<ImageView> &field) override;

    std::tuple<int, std::string, double> classifyChars(std::shared_ptr<ImageView> &field,
                                                       std::shared_ptr<ClassifierInterface> &classifier) override;

    static std::shared_ptr<CharSegmenterInterface> makeSegmenter(int char_type, const OcrParams &params);
};

} // namespace vdoc

#endif // VOCR_WORDINATORSIMPLE_H
