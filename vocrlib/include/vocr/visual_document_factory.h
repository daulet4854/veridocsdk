#ifndef VOCR_VISUAL_DOCUMENT_FACTORY_H
#define VOCR_VISUAL_DOCUMENT_FACTORY_H

#include "document_meta.h"
#include "image_view.h"
#include "view.h"
#include "vocr/visual_document.h"
#include <vocr/doc_type.h>

#include <nlohmann/json.hpp>

namespace vdoc {
class VisualDocumentFactory {
  protected:
    /**
     * Assigns OCR parameter values to a given 'view' (Field, TextField, Line) from a 'node'
     * @param view
     * @param node
     */
    static void setOcrParams(const std::shared_ptr<vdoc::View> &view, const nlohmann::json &node);

    static std::shared_ptr<vdoc::View> getField(nlohmann::json node);

    static void traverse(std::shared_ptr<vdoc::View> root, nlohmann::json meta);

    static bool getIfExistInt(std::string field_name, const nlohmann::json &node, int &result);

  public:
    static std::shared_ptr<vdoc::VisualDocument> create(const vdoc::DocType &doc_type, const cv::Mat &src);
};

} // namespace vdoc
#endif // VOCR_VISUAL_DOCUMENT_FACTORY_H
