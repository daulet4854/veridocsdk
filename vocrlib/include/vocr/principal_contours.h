//
// Created by Kanat on 20/06/18.
//

/*
DESCRIPTION OF TEXT CONTOURS TEMPLATE BASED DOCUMENT LOCALIZATION

    This document localization method uses a fact that the most documents have texts that are located
in fixed positions. Knowing this, one can use templates of fixed texts to estimate position of the whole document.
Text contours template features are rotation and scale invariant. But in order to use these features with different
rotation cases, text detection step should also be able to detect texts in different rotations.

    The main text contour, on which search of other contours depends, is called a "baseContour". For better performace
of the method, one should choose relatively large and fixed contour as the baseContour (in fact, all searched contours
should be large enough). "childContours" and "additionalContours" are contours that are located in permanent positions
related to baseContour and used to detect document by its text contour layout. "childContours" are considered to be
more reliable than "additionalContours". This is because childContours have more stable position and aspect ratio.
additionalContours can be of different aspect ratios and size, but all those possibilities must be annotated.
Generally, it is preferred to choose text contours that are located in fixed positions and don't have similar text
contours nearby. Text contours had better be of shape that will have minimum enclosing rectangle in only one way.
Because choosing text contour that can have several different minimum enclosing rectangles depending on image
conditions can result in incorrect angle for this text. childContours must be chosen to be distant from baseContour
in both X and Y axis as far as possible in order to improve detection accuracy.

    On the other hand, "additionalContours" are not obliged to have constant aspect ratio and to reside in a distant
location from baseContour. They serve as an extra verification purpose and for detection confidence rate estimation.

    Now I am scaling image to some predefined width and keep aspect ratio. For good text detection purpose,
chosen texts shouldn't be of very small size on the scaled image. According to visual observation by Kanat,
it is preferred that the height of the text contours is not less than 5 pixels (not tested thoroughly).

Further JSON annotation is explained:

Document templates in main array of JSON below:
 - stores a list of document templates;
 - each document template has:
    - documentType, which identifies type and side of the document;
    - xCoordinate and yCoordinate are x and y coordinates of document relative to the center of baseContour.
      Their values are given as a ratio of according distances to the width of baseContour.
    - width and height fields are width and height of the document. Their values are given as a ratio of exact
      width and height values of the document to the width of baseContour.

baseContour:
 - should have long width (maybe the longest among chosen contours);
 - aspectRatio is the ratio of width of the contour to its height;
 - contourName is a string value for distinguishing contours if necessary;

childContours:
 - xGapFromBase and yGapFromBase are the gaps from the center of the baseContour to the center of this contour in x
   and y directions accordingly. Their values are given as the ratios of gap distances to the width of baseContour;
 - ratioOfWidths is the ratio of width of this contour to the width of the baseContour;
 - aspectRatio is the ratio of width of the contour to its height;
 - contourName is a string value for distinguishing contours if necessary;

additionalContours:
 - contourName is a string value for distinguishing contours if necessary;
 - xGapFromBase and yGapFromBase are X and Y gaps of checkPoint from center of baseContour. Given in gap distance to
   baseContour width ratio;
 - aspectRatio (optional) is the ratio of width of the contour to its height;
 - ratioOfHeights (optional) is the ratio of the height of this contour to the height of baseContour;
 - ratioOfWidths (optional) is the ratio of width of this contour to the width of the baseContour;
 - checkPoint is the corner position which is always located in the same place within the document.
   Possible values are:
    - center;
    - topLeft;
    - topRight;
 - checkType means how this contour should be checked and relation to baseContour.
   Possible values:
    - existOnly (check if this contour exists in the required location);
    - parallel (check existence and parallelism with baseContour);

*/

#ifndef VOCR_PRINCIPAL_CONTOURS_H
#define VOCR_PRINCIPAL_CONTOURS_H

#include <nlohmann/json.hpp>

static nlohmann::json contour_templates_ = R"(
    [
        {
            "documentType": "KZ_ID_4_FRONT",
            "xCoordinate": -1.0599,
            "yCoordinate": -2.1232,
            "width": 3.5634,
            "height": 2.257,
            "baseContour": {
                "contourName": "IIN",
                "aspectRatio": 9.4667
            },
            "childContours": [
                {
                    "contourName": "RESPUBLIKA KAZAKHSTAN",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 19.2632,
                    "xGapFromBase": 1.6901,
                    "yGapFromBase": -1.882,
                    "ratioOfWidths": 1.2887
                },
                {
                    "contourName": "ZHEKE KUALIK",
                    "aspectRatio": 10.7059,
                    "xGapFromBase": -0.2394,
                    "yGapFromBase": -1.7482,
                    "ratioOfWidths": 0.6408
                }
            ],
            "additionalContours": [
                {
                    "contourName": "dateOfBirthField",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 6.36,
                    "xGapFromBase": 0.6004,
                    "yGapFromBase": -0.3468,
                    "ratioOfWidths": 0.5599
                },
                {
                    "contourName": "First_name_field",
                    "checkType": "existOnly",
                    "checkPoint": "topLeft",
                    "xGapFromBase": 0.3204,
                    "yGapFromBase": -1.3063,
                    "ratioOfHeights": 0.8
                },
                {
                    "contourName": "Last_name_field",
                    "checkType": "existOnly",
                    "checkPoint": "topLeft",
                    "xGapFromBase": 0.3204,
                    "yGapFromBase": -1.007,
                    "ratioOfHeights": 0.8
                }
            ]
        },
        {
            "documentType": "KZ_ID_4_BACK",
            "xCoordinate": -3.2205,
            "yCoordinate": -0.2106,
            "width": 3.9803,
            "height": 2.5197,
            "baseContour": {
                "contourName": "ID NUMBER",
                "aspectRatio": 7.697
            },
            "childContours": [
                {
                    "contourName": "MrzLowest",
                    "aspectRatio": 32.3571,
                    "xGapFromBase": -1.2362,
                    "yGapFromBase": 1.9941,
                    "ratioOfWidths": 3.5669
                },
                {
                    "contourName": "Expire_date_field",
                    "aspectRatio": 6.36,
                    "xGapFromBase": -0.939,
                    "yGapFromBase": 1.126,
                    "ratioOfWidths": 0.626
                }
            ],
            "additionalContours": [
                {
                    "contourName": "MrzMiddle",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 31.2759,
                    "xGapFromBase": -1.2362,
                    "yGapFromBase": 1.7303,
                    "ratioOfWidths": 3.5709
                },
                {
                    "contourName": "MrzUpper",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 31.1724,
                    "xGapFromBase": -1.2362,
                    "yGapFromBase": 1.4764,
                    "ratioOfWidths": 3.5591
                },
                {
                    "contourName": "Given_date_field",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 6.36,
                    "xGapFromBase": -1.7579,
                    "yGapFromBase": 1.126,
                    "ratioOfWidths": 0.626
                },
                {
                    "contourName": "Birth_place_field",
                    "checkType": "existOnly",
                    "checkPoint": "topLeft",
                    "xGapFromBase": -2.0748,
                    "yGapFromBase": 0.313,
                    "ratioOfHeights": 0.697
                }
            ]   
        },
        {
            "documentType": "KZ_ID_3_FRONT",
            "xCoordinate": -4.1952,
            "yCoordinate": -2.7246,
            "width": 5.3957,
            "height": 3.4011,
            "baseContour": {
                "contourName": "IIN",
                "aspectRatio": 8.9048
            },
            "childContours": [
                {
                    "contourName": "Respublikasy",
                    "aspectRatio": 12,
                    "xGapFromBase": -2.623,
                    "yGapFromBase": -2.1551,
                    "ratioOfWidths": 1.3476
                },
                {
                    "contourName": "KAZAKHSTAN ON LEFT",
                    "aspectRatio": 7.4583,
                    "xGapFromBase": -2.6257,
                    "yGapFromBase": -2.3235,
                    "ratioOfWidths": 0.9572
                },
                {
                    "contourName": "KAZAKHSTAN ON RIGHT",
                    "aspectRatio": 8.4762,
                    "xGapFromBase": -0.4465,
                    "yGapFromBase": -2.1551,
                    "ratioOfWidths": 0.9519
                }
            ],
            "additionalContours": [
                {
                    "contourName": "FirsNameField",
                    "checkType": "existOnly",
                    "checkPoint": "topLeft",
                    "xGapFromBase": -2.3235,
                    "yGapFromBase": -1.1845,
                    "ratioOfHeights": 1
                },
                {
                    "contourName": "LastNameField",
                    "checkType": "existOnly",
                    "checkPoint": "topLeft",
                    "xGapFromBase": -2.3182,
                    "yGapFromBase": -0.8155,
                    "ratioOfHeights": 1
                },
                {
                    "contourName": "DateOfBirthField",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 6.7619,
                    "xGapFromBase": -1.9492,
                    "yGapFromBase": -0.0374,
                    "ratioOfWidths": 0.7594
                }
            ]  
        },
        {
            "documentType": "KZ_ID_3_BACK",
            "xCoordinate": -0.5481,
            "yCoordinate": -0.628,
            "width": 1.1061,
            "height": 0.6958,
            "baseContour": {
                "contourName": "MrzLower",
                "aspectRatio": 32.6429
            },
            "childContours": [
                {
                    "contourName": "MrzUpper",
                    "aspectRatio": 29.4194,
                    "xGapFromBase": 0.0011,
                    "yGapFromBase": -0.1417,
                    "ratioOfWidths": 0.9978
                },
                {
                    "contourName": "IdNumber",
                    "aspectRatio": 7.4231,
                    "xGapFromBase": 0.3091,
                    "yGapFromBase": -0.5361,
                    "ratioOfWidths": 0.2112
                }
            ],
            "additionalContours": [
                {
                    "contourName": "MrzMiddle",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 30.5,
                    "xGapFromBase": 0.0005,
                    "yGapFromBase": -0.0711,
                    "ratioOfWidths": 1.0011
                },
                {
                    "contourName": "GivenDate",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 6.4545,
                    "xGapFromBase": -0.1422,
                    "yGapFromBase": -0.2538,
                    "ratioOfWidths": 0.1554
                },
                {
                    "contourName": "ExpirationDate",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 6.4545,
                    "xGapFromBase": 0.0744,
                    "yGapFromBase": -0.2538,
                    "ratioOfWidths": 0.1554
                }
            ]   
        },
        {
            "documentType": "KZ_VEHICLEREGCERT_2_FRONT",
            "xCoordinate": -1.5984,
            "yCoordinate": -2.0708,
            "width": 3.0445,
            "height": 2.1779,
            "baseContour": {
                "contourName": "id_number",
                "aspectRatio": 11.4154
            },
            "childContours": [
                {
                    "contourName": "3_lines_on_top",
                    "aspectRatio": 7.4456,
                    "xGapFromBase": -0.1341,
                    "yGapFromBase": -1.8747,
                    "ratioOfWidths": 1.9367
                },
                {
                    "contourName": "Certificate",
                    "aspectRatio": 23.5192,
                    "xGapFromBase": -0.1503,
                    "yGapFromBase": -1.7796,
                    "ratioOfWidths": 1.6482
                },
                {
                    "contourName": "Tusi_Svet",
                    "aspectRatio": 4.8793,
                    "xGapFromBase": -1.3322,
                    "yGapFromBase": -0.5411,
                    "ratioOfWidths": 0.3814
                }
            ],
            "additionalContours": [
                {
                    "contourName": "Ondirushi_field",
                    "checkType": "existOnly",
                    "checkPoint": "topRight",
                    "xGapFromBase": 1.2978,
                    "yGapFromBase": -0.7163,
                    "ratioOfHeights": 0.8769
                },
                {
                    "contourName": "Ondirushi",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 17.614,
                    "xGapFromBase": -0.8437,
                    "yGapFromBase": -0.6671,
                    "ratioOfWidths": 1.3531
                },
                {
                    "contourName": "Zhuktemesiz",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 23.2456,
                    "xGapFromBase": -0.6287,
                    "yGapFromBase": -0.128,
                    "ratioOfWidths": 1.7857
                },
                {
                    "contourName": "Kolik_sanaty",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 14.3036,
                    "xGapFromBase": -0.9771,
                    "yGapFromBase": -1.2096,
                    "ratioOfWidths": 1.0795
                }
            ]
        },
        {
            "documentType": "KZ_VEHICLEREGCERT_2_BACK",
            "xCoordinate": -0.6416,
            "yCoordinate": -1.0109,
            "width": 4.7843,
            "height": 3.5425,
            "baseContour": {
                "contourName": "Oblys_Obalst",
                "aspectRatio": 8.1964
            },
            "childContours": [
                {
                    "contourName": "Id_number",
                    "aspectRatio": 11.6094,
                    "xGapFromBase": 2.0131,
                    "yGapFromBase": 2.3508,
                    "ratioOfWidths": 1.6187
                },
                {
                    "contourName": "Respublika",
                    "aspectRatio": 9.8824,
                    "xGapFromBase": 2.951,
                    "yGapFromBase": -0.8355,
                    "ratioOfWidths": 1.098
                }
            ],
            "additionalContours": [
                {
                    "contourName": "Iesi_field",
                    "checkType": "existOnly",
                    "checkPoint": "topRight",
                    "xGapFromBase": 4.1078,
                    "yGapFromBase": -0.4946
                },
                {
                    "contourName": "Audan_field",
                    "checkType": "existOnly",
                    "checkPoint": "topRight",
                    "xGapFromBase": 3.9031,
                    "yGapFromBase": 0.1612
                },
                {
                    "contourName": "Oblys_field",
                    "checkType": "existOnly",
                    "checkPoint": "topRight",
                    "xGapFromBase": 3.8965,
                    "yGapFromBase": -0.0436
                }
            ]
        },
        {
            "documentType": "KZ_VEHICLEREGCERT_1_FRONT",
            "xCoordinate": -1.9921,
            "yCoordinate": -2.4101,
            "width": 3.7778,
            "height": 2.5291,
            "baseContour": {
                "contourName": "id_number",
                "aspectRatio": 11.1176
            },
            "childContours": [
                {
                    "contourName": "3_lines_on_top",
                    "aspectRatio": 7.6441,
                    "xGapFromBase": -0.1852,
                    "yGapFromBase": -2.1905,
                    "ratioOfWidths": 2.3862
                },
                {
                    "contourName": "Certificat",
                    "aspectRatio": 22.6471,
                    "xGapFromBase": -0.2011,
                    "yGapFromBase": -2.0794,
                    "ratioOfWidths": 2.037
                },
                {
                    "contourName": "Tusi_Svet",
                    "aspectRatio": 5.2941,
                    "xGapFromBase": -1.664,
                    "yGapFromBase": -0.619,
                    "ratioOfWidths": 0.4762
                }
            ],
            "additionalContours": [
                {
                    "contourName": "Zhuksiz",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 20.7778,
                    "xGapFromBase": -0.9127,
                    "yGapFromBase": -0.1349,
                    "ratioOfWidths": 1.9788
                },
                {
                    "contourName": "Shanaq",
                    "checkType": "parallel",
                    "checkPoint": "center",
                    "aspectRatio": 10,
                    "xGapFromBase": -1.4259,
                    "yGapFromBase": -0.7698,
                    "ratioOfWidths": 0.9524
                },
                {
                    "contourName": "LicensePlateField",
                    "checkType": "parallel",
                    "checkPoint": "topRight",
                    "xGapFromBase": 1.7487,
                    "yGapFromBase": -1.5899
                }
            ]
        },
        {
            "documentType": "KZ_VEHICLEREGCERT_1_BACK",
            "xCoordinate": -0.6416,
            "yCoordinate": -1.0109,
            "width": 4.7843,
            "height": 3.5425,
            "baseContour": {
                "contourName": "Oblys_Obalst",
                "aspectRatio": 8.1964
            },
            "childContours": [
                {
                    "contourName": "Id_number",
                    "aspectRatio": 11.6094,
                    "xGapFromBase": 2.0131,
                    "yGapFromBase": 2.3508,
                    "ratioOfWidths": 1.6187
                },
                {
                    "contourName": "Respublika",
                    "aspectRatio": 9.8824,
                    "xGapFromBase": 2.951,
                    "yGapFromBase": -0.8355,
                    "ratioOfWidths": 1.098
                }
            ],
            "additionalContours": [
                {
                    "contourName": "Iesi_field",
                    "checkType": "existOnly",
                    "checkPoint": "topRight",
                    "xGapFromBase": 4.1078,
                    "yGapFromBase": -0.4946
                },
                {
                    "contourName": "Audan_field",
                    "checkType": "existOnly",
                    "checkPoint": "topRight",
                    "xGapFromBase": 3.9031,
                    "yGapFromBase": 0.1612
                },
                {
                    "contourName": "Oblys_field",
                    "checkType": "existOnly",
                    "checkPoint": "topRight",
                    "xGapFromBase": 3.8965,
                    "yGapFromBase": -0.0436
                }
            ]
        },
        {
            "additionalContours": [
                {
                    "aspectRatio": 33.44444274902344,
                    "checkPoint": "center",
                    "checkType": "parallel",
                    "contourName": "Upper_Mrz",
                    "ratioOfWidths": 0.9977900385856628,
                    "xGapFromBase": 0.0011049723252654076,
                    "yGapFromBase": -0.06685082614421844
                },
                {
                    "aspectRatio": 8.086956977844238,
                    "checkPoint": "center",
                    "checkType": "parallel",
                    "contourName": "IIN",
                    "ratioOfWidths": 0.20552486181259155,
                    "xGapFromBase": -0.0027624310459941626,
                    "yGapFromBase": -0.15524861216545105
                },
                {
                    "checkPoint": "center",
                    "checkType": "parallel",
                    "contourName": "Kazakhstan_Kaz",
                    "ratioOfWidths": 0.15690608322620392,
                    "xGapFromBase": -0.006077348254621029,
                    "yGapFromBase": -0.6408839821815491
                }
            ],
            "baseContour": {
                "aspectRatio": 30.16666603088379,
                "contourName": "Lower_Mrz"
            },
            "childContours": [
                {
                    "aspectRatio": 26.33333396911621,
                    "contourName": "Zheke_Kualik",
                    "ratioOfWidths": 0.6110497117042542,
                    "xGapFromBase": 0.1966850757598877,
                    "yGapFromBase": -0.485635370016098
                },
                {
                    "aspectRatio": 6.08695650100708,
                    "contourName": "Tugan_Kun_Field",
                    "ratioOfWidths": 0.15469613671302795,
                    "xGapFromBase": -0.027071822434663773,
                    "yGapFromBase": -0.2093922644853592
                }
            ],
            "documentType": "KZ_ID_2_FRONT",
            "height": 0.7646408677101135,
            "width": 1.1049723625183105,
            "xCoordinate": -0.5585635304450989,
            "yCoordinate": -0.6961326003074646
        },
        {
            "additionalContours": [
                {
                    "checkPoint": "topLeft",
                    "checkType": "existOnly",
                    "contourName": "Tugan_Zher_Field",
                    "xGapFromBase": -3.432126760482788,
                    "yGapFromBase": 0.46606335043907166
                },
                {
                    "checkPoint": "topLeft",
                    "checkType": "existOnly",
                    "contourName": "Ulty_field",
                    "xGapFromBase": -3.4366514682769775,
                    "yGapFromBase": 0.7420814633369446
                },
                {
                    "checkPoint": "topLeft",
                    "checkType": "existOnly",
                    "contourName": "Mekeme_field",
                    "xGapFromBase": -3.4276018142700195,
                    "yGapFromBase": 1.6018099784851074
                },
                {
                    "checkPoint": "topLeft",
                    "checkType": "existOnly",
                    "contourName": "Turgylykty_field",
                    "xGapFromBase": -3.4366514682769775,
                    "yGapFromBase": 1.0226244926452637
                }
            ],
            "baseContour": {
                "aspectRatio": 7.366666793823242,
                "contourName": "id_number"
            },
            "childContours": [
                {
                    "aspectRatio": 23.238094329833984,
                    "contourName": "Kualik_bergen",
                    "ratioOfWidths": 2.2081449031829834,
                    "xGapFromBase": -2.328054189682007,
                    "yGapFromBase": 1.5180995464324951
                },
                {
                    "aspectRatio": 13.34782600402832,
                    "contourName": "Date_From_To",
                    "ratioOfWidths": 1.389140248298645,
                    "xGapFromBase": -2.7239818572998047,
                    "yGapFromBase": 1.9343891143798828
                }
            ],
            "documentType": "KZ_ID_2_BACK",
            "height": 3.135746717453003,
            "width": 4.5248870849609375,
            "xCoordinate": -3.6674208641052246,
            "yCoordinate": -0.33484163880348206
        },
        {
            "additionalContours": [
                {
                    "aspectRatio": 12.411765098571777,
                    "checkPoint": "center",
                    "checkType": "parallel",
                    "contourName": "driving_license",
                    "ratioOfWidths": 0.9094827771186829,
                    "xGapFromBase": -0.045258618891239166,
                    "yGapFromBase": 0.2392241358757019
                }
            ],
            "baseContour": {
                "aspectRatio": 12.88888931274414,
                "contourName": "zhurgizushi_kualigi"
            },
            "childContours": [
                {
                    "aspectRatio": 6.692307472229004,
                    "contourName": "date_from",
                    "ratioOfWidths": 0.75,
                    "xGapFromBase": -1.125,
                    "yGapFromBase": 1.1939655542373657
                },
                {
                    "aspectRatio": 6.44444465637207,
                    "contourName": "date_until",
                    "ratioOfWidths": 0.75,
                    "xGapFromBase": -0.27586206793785095,
                    "yGapFromBase": 1.1918103694915771
                }
            ],
            "documentType": "KZ_DRIVER_LICENCE_2",
            "height": 2.719827651977539,
            "width": 4.310344696044922,
            "xCoordinate": -2.9784483909606934,
            "yCoordinate": -0.12931033968925476
        },
        {
            "additionalContours": [
                {
                    "aspectRatio": 10.239999771118164,
                    "checkPoint": "center",
                    "checkType": "parallel",
                    "contourName": "permis_de_conduire",
                    "ratioOfWidths": 0.656410276889801,
                    "xGapFromBase": -0.9307692050933838,
                    "yGapFromBase": 0.0
                },
                {
                    "checkPoint": "topLeft",
                    "checkType": "existOnly",
                    "contourName": "surname",
                    "xGapFromBase": -1.9128204584121704,
                    "yGapFromBase": 0.9628205299377441
                }
            ],
            "baseContour": {
                "aspectRatio": 15.600000381469727,
                "contourName": "voditelskoe_udostoverenie"
            },
            "childContours": [
                {
                    "aspectRatio": 6.0,
                    "contourName": "issue_date",
                    "ratioOfWidths": 0.38461539149284363,
                    "xGapFromBase": -0.8538461327552795,
                    "yGapFromBase": 0.5205128192901611
                },
                {
                    "aspectRatio": 4.974999904632568,
                    "contourName": "category_table",
                    "ratioOfWidths": 0.5102564096450806,
                    "xGapFromBase": -0.7705128192901611,
                    "yGapFromBase": 0.13717949390411377
                }
            ],
            "documentType": "KZ_DRIVER_LICENCE_1",
            "height": 1.6179487705230713,
            "width": 2.5641026496887207,
            "xCoordinate": -2.0051281452178955,
            "yCoordinate": -0.23974359035491943
        }
    ]
)"_json;

#endif