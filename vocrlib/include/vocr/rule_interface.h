#ifndef VOCR_RULE_INTERFACE_H
#define VOCR_RULE_INTERFACE_H

#include <memory>

#include "visual_document.h"

namespace vdoc {

/**
 * Interface for post-processing rules
 */
class RuleInterface {
  public:
    /**
     * Will apply rule to visual document
     *
     * @param vis_doc visual document where to apply rules
     */
    virtual void apply(std::shared_ptr<vdoc::VisualDocument> &vis_doc) = 0;

    virtual ~RuleInterface(){};
};
} // namespace vdoc

#endif // VOCR_RULE_INTERFACE_H
