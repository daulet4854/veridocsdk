//
// Created by chingy on 11/22/17.
//

#ifndef VOCR_CLASSIFIER_INTERFACE_H
#define VOCR_CLASSIFIER_INTERFACE_H

#include <opencv2/core/mat.hpp>
#include <opencv2/dnn.hpp>
#include <vector>

namespace vdoc {

class ClassifierInterface {
  public:
    /**
     * Sets the model which will be used for classification of characters
     * @param net
     */
    //    virtual void setModel(const cv::dnn::Net &net) = 0;

    /**
     * This method can be used for preparing images for classification. Possible operation
     * is resizing images to necessary dimensions.
     * @param chars
     */
    virtual void prepareData(const std::vector<cv::Mat> &chars) = 0;

    /**
     * Predicts classes of images. Data is set by PrepareData method.
     * @returns Vector of 'label<->probability' pairs
     */
    virtual std::vector<std::pair<std::string, double>> predict(const int &field_type) = 0;

    /**
     * Method that corrects prediction depending on prior information. If a letter appears in the digit only
     * field it is translated into closest digit
     * @return
     */
    virtual void correctPrediction(std::vector<std::pair<std::string, double>> &preds, const int &type) = 0;

    /**
     * Corrects confidence scores if there were some correcions. For example, 0 is usually misclassified as O,
     * if it is corrected then it's confidence score is also corrected to 0.95.
     * @param preds
     * @param type
     */
    virtual void correctConfidence(std::vector<std::pair<std::string, double>> &preds, const int &type) = 0;

    virtual ~ClassifierInterface(){};
};
} // namespace vdoc

#endif // VOCR_CLASSIFIER_INTERFACE_H
