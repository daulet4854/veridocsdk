#ifndef VOCR_VISDOC_VISUAL_DOCUMENT_H
#define VOCR_VISDOC_VISUAL_DOCUMENT_H

#include "document_meta.h"
#include "image_view.h"
#include <base64/base64.h>
#include <utfcpp/utf8.h>
#include <vocr/doc_type.h>

namespace vdoc {
class VisualDocument : public ImageView {

  protected:
    /**
     * The document type
     */
    vdoc::DocType doc_type_;

    /**
     * The results of recognition (key => predicted_value/confidence_score)
     */
    std::map<std::string, std::tuple<int, std::string, double>> predicted_result_;

    /**
     * Special height value to which we resize image before recognition
     * IMPORTANT!!! this height value is used in text_detector_naive
     * Its change requires change of all minHeightRatio coefficients in layouts.json
     * The results of recognition (key => predicted_value/confidence_score)
     */
    static const int vis_doc_recognition_height_;

  public:
    VisualDocument() {
        children_ = {};
        doc_type_ = vdoc::DocType::UNKNOWN;
    }

    VisualDocument(const vdoc::DocType type, const cv::Mat &image) {
        auto doctyoe_id = static_cast<int>(type);
        double aspect_ratio = document_meta_[doctyoe_id]["aspect_ratio"];
        children_ = {};
        doc_type_ = type;
        height_ = vis_doc_recognition_height_;
        width_ = static_cast<int>(height_ * aspect_ratio);
        VisualDocument::setImage(image);
    }

    static int getVisDocRecognitionHeight();

    /**
     * The document type
     */
    vdoc::DocType getDocumentType();

    /**
     * Sets the document type
     */
    void setDocumentType(DocType doctype);

    /**
     * Results of recognition
     */
    std::map<std::string, std::tuple<int, std::string, double>> getPredictedResult();

    std::string getPredictedResult(const std::string &key) const;

    std::wstring getPredictedResultUTF16(const std::string &key) const;

    void setPredictedResult(const std::string &key, std::tuple<int, std::string, double> value);

    void setPredictedResult(const std::string &key, const std::string &value);

    void setPredictedResult(const std::string &key, const std::string &value, int char_type);

    void setPredictedResult(const std::string &key, const std::string &value, int char_type, double confidence_score);

    void setPredictedResult(const std::string &key, std::wstring wvalue);

    void setPredictedResult(const std::string &key, std::wstring wvalue, int char_type);

    void setPredictedResult(const std::string &key, std::wstring wvalue, int char_type, double confidence_score);

    void setPredictedResult(const std::string &key, const cv::Mat &mat);

    double getConfidenceScore(const std::string &key) const;

    void setConfidenceScore(const std::string &key, double confidence_score_);

    int getCharacterType(const std::string &key) const;

    /**
     * Forbid the cv::Mat size manipulation methods for vdoc::VisualDocument.
     * Trying to call one of the methods below will produce a compiler error
     * IMPORTANT!
     * -----------------------------------------------------------------------------
     * The base class’s method can still be called by qualifying with the base class
     * identifier: visual_document_instance.View::setWidth(value)
     * -----------------------------------------------------------------------------
     */
    void setWidth(const int width_) = delete;
    void setHeight(const int height_) = delete;
    void setSize(cv::Size size_) = delete;

    /**
     * Set current @class{cv::Mat} object
     * @param &image_
     */
    void setImage(const cv::Mat &image);

    void apply(class VisDocProcessor *processor) override;
};

} // namespace vdoc
#endif // VOCR_VISUAL_DOCUMENT_H
