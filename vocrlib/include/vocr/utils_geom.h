//
// Created by chingy on 1/5/18.
//

#ifndef VOCR_UTILS_GEOM_H
#define VOCR_UTILS_GEOM_H

#include <opencv2/core/types.hpp>

namespace vdoc {
namespace geom {
/**
 * Given a set of points for each corner of the possible rectangles this method tries to find best rectangle.
 * Number of points for each corner must be equal, otherwise returns emptt array
 * @param threshold_score some score below which a shape is not considered as a rectangle
 * @param corners Tuple of <top-right, top-left, bottom-right, bottom-left> points
 * @param aspect_ratio perfect aspect ratio
 * @return
 */
std::vector<cv::Point> bestRectangle(const double &threshold_score, const std::vector<cv::Point> &top_left,
                                     const std::vector<cv::Point> &top_right,
                                     const std::vector<cv::Point> &bottom_right,
                                     const std::vector<cv::Point> &bottom_left, const double &aspect_ratio);

/**
 * Calculates distance between two given points
 * @param p1 First point
 * @param p2 Second point
 * @return distance
 */
int distance(const cv::Point &p1, const cv::Point &p2);

/**
 * Extracts lines from the given image patch. Lines represented as two points
 * @param patch
 * @param offset Translates coordinates of the lines relative to the origin given by offset.
 * @param vote_threshold Min number of votes line need to get
 * @param max_lines maximum number of lines to be returned
 * @return Array of line. Each line is given by two points on it.
 */
std::vector<std::pair<cv::Point, cv::Point>> getLines(const cv::Mat &patch, const int &vote_threshold,
                                                      const int &max_lines = 5, const cv::Point &offset = {0, 0});

/**
 * Calculates intersection of two line given by two points on each one
 * @param o1
 * @param p1
 * @param o2
 * @param p2
 * @param r
 * @return
 */
bool intersection(const cv::Point2f &o1, const cv::Point2f &p1, const cv::Point2f &o2, const cv::Point2f &p2,
                  cv::Point2f &intersect);

/**
 * Given two sets of lines it calculates all intersections of all combinations of lines from two sets
 * @param set_lines_1
 * @param set_lines_2
 */
std::vector<cv::Point> allPossibleIntersections(const std::vector<std::pair<cv::Point, cv::Point>> &set_lines_1,
                                                const std::vector<std::pair<cv::Point, cv::Point>> &set_lines_2);

float degreeToRadian(const float degrees);

/**
 * Given two cv::RotatedRect, it will calculate their intersection area.
 * Note: this is a wrapper around OpenCV's BUGGY `rotatedRectangleIntersection()`. It tries to correct these bugs.
 *
 * @param rect1 Rectangle 1
 * @param rect2 Rectangle 2
 * @return double intersection area of rect1 and rect2
 */
double rotatedRectangleIntersectionArea(const cv::RotatedRect &rect1, const cv::RotatedRect &rect2);

/**
 * Given cv::RotatedRect, it will set largest side to width and update angle if width changes
 */
void correctRotRectAngle(cv::RotatedRect &rot_rect);
} // namespace geom
} // namespace vdoc

#endif // VOCR_UTILS_GEOM_H
