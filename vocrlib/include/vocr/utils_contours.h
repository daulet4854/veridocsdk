#ifndef VOCR_UTILS_CONTOURS_H
#define VOCR_UTILS_CONTOURS_H

#include <iostream>
#include <set>
#include <vector>

#include <opencv2/core/types.hpp>

#include <vocr/utils_geom.h>

namespace vdoc {
typedef std::vector<cv::Point> Contour;
typedef std::vector<Contour> ContoursList;

/**
 * Helper functions for working with contours
 */
namespace cntrs {

/**
 * Filters provided contours by their apsect ratio.
 * @param input
 * @param min_ar Min allowed aspect ratio
 * @param max_ar Max allowed aspect ratio
 * @param filtered Output filtered contours
 * @param bboxes Corresponding bounding boxes (min bounding rectangles)
 */
void filterByAspectRatio(const vdoc::ContoursList &input, const double &min_ar, const double &max_ar,
                         vdoc::ContoursList &filtered, std::vector<cv::RotatedRect> &bboxes);

/**
 * Filters providing bounding boxes of the contours by its width.
 * @param bboxes Input boxes
 * @param min_width Min width of the bounding box to pass from the filter
 * @param output Filtered array of bounding boxes
 * @return Corresponding indexes of contours in the input array
 */
std::vector<int> filterByMinWidth(const std::vector<cv::RotatedRect> &bboxes, const int &min_width,
                                  std::vector<cv::RotatedRect> &output);

/**
 * Filters providing bounding boxes of the contours by its width.
 * @param bboxes Input boxes
 * @param min_width Min width of the bounding box to pass from the filter
 * @param output Filtered array of bounding boxes
 * @return Corresponding indexes of contours in the input array
 */
std::vector<int> filterByMinWidth(const std::vector<cv::Rect> &bboxes, const int &min_width,
                                  std::vector<cv::Rect> &output);

/**
 * Filters providing bounding boxes of the contours by its height.
 * @param bboxes Input boxes
 * @param min_height Min height of the bounding box to pass from the filter
 * @param output Filtered array of bounding boxes
 * @return Corresponding indexes of contours in the input array
 */
std::vector<int> filterByMinHeight(const std::vector<cv::Rect> &bboxes, const int &min_height,
                                   std::vector<cv::Rect> &output);

/**
 * Returns only contours aligned vertically
 * @param bboxes
 * @param max_displace
 * @param output
 * @return Indexes of contours in the original list
 */
std::vector<int> filterByVerticalAlignment(const std::vector<cv::RotatedRect> &bboxes, const int &max_displace,
                                           std::vector<cv::RotatedRect> &output);

/**
 * Returns only contours that have almost equal width
 * @param bboxes
 * @param max_displace
 * @param output
 * @return Indexes of contours in the original list
 */
std::vector<int> filterBySizeSimilarity(const std::vector<cv::RotatedRect> &bboxes, const int &thresh,
                                        std::vector<cv::RotatedRect> &output);

/**
 * Eliminate those contours that are too close to the side borders of an image
 * @param bboxes - Rotated rectangles (bounding boxes) of the contours
 * @param width  -  width of the image
 * @param padding - Relative padding from both sides (ex. 0.2)
 * @param output  - Filtered contours
 * @return Ror each contour in the resulting list returns index in the original list
 */
std::vector<int> filterBySidePaddings(const std::vector<cv::RotatedRect> &bboxes, const int &width,
                                      const double &padding, std::vector<cv::RotatedRect> &output);

/**
 * Contours presented as bounding rectangles. Method sorts rectangles based on the
 * X coordinate from left to right
 * @param contours
 * @return Indexes of contours in the original list
 */
std::vector<int> orderContoursHorizontally(std::vector<cv::Rect> &contours);

/**
 * Contours presented as bounding rectangles. Method sorts rectangles based on the
 * Y coordinate from UP to BOTTOM
 * @param contours
 * @return Indexes of contours in the original list
 */
std::vector<int> orderContoursVert(std::vector<cv::Rect> &contours);

/**
 * Contours presented as bounding rectangles. Method sorts rectangles based on the
 * Y coordinate from UP to BOTTOM
 * @param contours
 * @return Indexes of contours in the original list
 */
std::vector<int> orderContoursVert(std::vector<cv::RotatedRect> &contours);

/**
 * Contours presented as bounding rectangles. Method sorts rectangles based on their width
 * from small to big.
 * @param contours
 * @return Indexes of contours in the original list
 */
std::vector<int> orderContoursByWidth(std::vector<cv::Rect> &contours);

/**
 * Orders rectangle vertices clockwise starting from upper left corner.
 * @param points - Unordered 4 vertices of the rectangle (mutable)
 */
void orderRectVertices(std::vector<cv::Point2f> &points);

/**
 * Orders rectangle vertices clockwise starting from upper left corner.
 * @param points - Unordered 4 vertices of the rectangle (mutable)
 */
void orderRectVertices(cv::Point2f *points);

/**
 * Orders rectangle vertices clockwise starting from upper left corner.
 * @param rect - Rotated rectangle
 * @return
 */
std::vector<cv::Point2f> orderedVertices(const cv::RotatedRect &rect);

/**
 * Given vertically sorted contours, this method would remove overlapping contours (e.g. from 2 different kernels). If
 * any 2 contours overlap it will choose the largest one.
 * Any 2 contours overlap iff intersection area is larger than thres * the area of the smallest contour.
 * NOTE: Max is 2 overlaps only!
 *
 * @param bboxes vertically sorted rotated rectangles of contours
 * @param thres threshold for overlap (min 0.0, max 1.0)
 * @param output filtered contours
 * @return std::set<int> set of indexes of filtered contours
 */
std::set<int> filterByOverlapping(const std::vector<cv::RotatedRect> &bboxes, double thres,
                                  std::vector<cv::RotatedRect> &output);

/**
 * Given vertically sorted contours, this method would remove any contours that are vertically too far away from the
 * rest of contours.
 * Contour is excluded iff distance between it's center and the center of the next contour is greater than thres *
 * height of the smallest contour.
 *
 * @param bboxes vertically sorted rotated rectangles of contours
 * @param thres threshold for exclusion (min 0.0, max -)
 * @param output filtered contours
 * @return std::set<int> set of indexes of filtered contours
 */
std::set<int> filterByVerticalDistanceGrouping(const std::vector<cv::RotatedRect> &bboxes, double thres,
                                               std::vector<cv::RotatedRect> &output);

std::vector<cv::Rect> divideIntoPartsAlongWidth(cv::Rect box, int num_parts);

} // namespace cntrs
} // namespace vdoc

#endif // VOCR_UTILS_CONTOURS_H
