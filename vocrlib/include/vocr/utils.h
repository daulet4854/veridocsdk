//
// Created by chingy on 10/30/17.
//

#ifndef VOCR_VOCR_UTIL_H
#define VOCR_VOCR_UTIL_H

#include <fstream>
#include <opencv2/core/mat.hpp>

namespace vdoc {

typedef std::pair<int, int> Segment;
typedef std::vector<Segment> Segments;

std::string base64Encode(unsigned char const *bytes_to_encode, unsigned int in_len);

/**
 * Crops horizontal row sub image containing the biggest row. Width of the cropped image equal to the original width.
 * While the height is less than the original.
 * @param img_thresh - Thresholded black and white binary image
 * @param img_row - output image
 * @param black_pixel - is a pixel that is counted as edge
 */
void SegmentTextRow(const cv::Mat &img_thresh, cv::Mat &img_row, cv::Rect &out_roi, const int &black_pixel);

/**
 * Given a row of a text this method calculates left and right boundaries of the presented text.
 * Height of the image remains the same, while the width will be decreased.
 * @param img_bin_text Input binary threshold image
 * @param dst The cropped resulting image
 * @param black_pixel - is a pixel that is counted as edge
 */
void SegmentTextHorizontally(const cv::Mat &img_bin_text, cv::Mat &dst, cv::Rect &roi, const int &black_pixel);

/**
 * Checks whether given rectangle boundaries are within the image
 * @param rect
 * @param img
 * @return
 */
bool CheckRectInImage(cv::Rect rect, cv::Mat img);

/**
 * Calculates bounding box for a field. Accepts black and white image after series of morphological
 * operations for highlighting areas of text
 * @param img_bin - Binary threshold image
 * @param black
 * @return
 */
cv::Rect GetFieldBoundaries(const cv::Mat &img_bin, const int &empty_space = 1, const int &black = 0);

/**
 * Calculates projections on x and y axes.
 * @param patch - some given image patch
 * @param xp - int vector, where projections on x (width) axes are stored
 * @param yp - int vector, where projections on y (height) axes are stored
 */
void CalcProjections(cv::Mat patch, std::vector<int> &xp, std::vector<int> &yp, const int &black_pixel = 0);

/**
 * Given a projection array this function divides the projection into segments. Segment it is part of
 * a projection array where all the values are greater or equal than predefined threshold ('thresh' argument).
 * Segment is represented with two numbers: the start and the end index on of a segment on projection array.
 * It is useful for determining rows of text in an image: first calculate projection on
 * y axis and then call this function to get the boundaries of text rows. Or for dividing a word into symbols.
 * @param projection Vector that contains projection values
 * @param thresh Threshold. Values in a segment are greater than the threshold.
 * @param margin Default value 0. Adds margins from both side of the segments. For example if a true width of a
 * segment 10 and margin is set to 2, then the final width of this segment will be
 * <margin> + <segment_width> + <margin> = 2 + 10 + 2 = 14
 * @return Returns a vector of segments. Segment is represented as a pair<int,int> with
 * the start and the end indexes.
 */
Segments GetSegments(std::vector<int> projection, int thresh, int margin = 0);

/**
 * Returns biggest segments in the array. number of returned segments is defined by 'num_segments'
 * @param segments
 * @param num_segments
 * @return
 */
Segments GetBiggestSegments(std::vector<std::pair<int, int>> segments, int num_segments);

/**
 * Returns the biggest segment in the provided list of segments
 * @param segments
 * @return
 */
Segment GetBiggestSegment(std::vector<std::pair<int, int>> segments);

/**
 * Lenght of a segment.
 * @param segment
 * @return segment: 3-5 -> length 3
 */
int SegmentLen(const Segment &segment);

/**
 * Sums values of two arrays element wise
 * @param proj1
 * @param proj2
 * @param result
 */
void JoinProjections(const std::vector<int> &proj1, const std::vector<int> &proj2, std::vector<int> &result);

/**
 * Calculate mean value of provided values
 * @param val
 * @return
 */
int Mean(std::vector<int> &val);

/**
 * Divides provided image patch into more smaller patches (vertical stripes) using lines provided in 'segments'
 * argument
 * @param patch Image which must be divided
 * @param segments X Coordinates of segments. Calculate it using 'GetSegments' function.
 * @return Vector of patches (cv::Mat)
 */
std::vector<cv::Mat> divideVertically(const cv::Mat &patch, const Segments &segments);

/**
 * Divide provided sgment into two equal parts
 * @param segment
 * @return
 */
Segments DivideSegmentByHalf(const Segment &segment);

/**
 * Divides the provided segment into number of equal parts
 * @param segment The segment to divide.
 * @param parts Number of parts.
 * @return
 */
Segments DivideSegment(const Segment &segment, const int &parts);

/**
 * Analyzes segments and split those the length of which greater than specific threshold value
 * @param segments - Results saved in the same array
 * @param thresh - If a segment greater than this threshold value, then divide the segment into smaller ones
 * @param value - Desired width of a segment. The final number of partitions will be calculated by dividing the length
 * of the segment by this value + 1.
 */
void splitSegmentsGreaterThan(std::vector<std::pair<int, int>> &segments, const int &thresh, const int &value);

/**
 * Converts rectangles to array of segments along X coordinate. e.g along width.
 * Joins overlapping rectangles
 * @param rects Rectangles that need to be converted to segments
 * @return Returns segments
 */
Segments rectsToSegments(const std::vector<cv::Rect> &rects);

/**
 * Filters segments by its lenght. If an array of segments, represented as a pair of stating and end points,
 * contains segment of lenght lower than given value i is removed from the array
 * @param segments - Segments to be filtered.
 * @param min_length All the segments below this value will be removed from the list
 * @return - returns only segments that passed the filter
 */
Segments FilterByLength(const Segments &segments, int min_length);

void grabID(const cv::Mat &img, cv::Mat &img_dst, double aspect_ratio);

bool intersection(cv::Point2f o1, cv::Point2f p1, cv::Point2f o2, cv::Point2f p2, cv::Point2f &r);

cv::Mat four_point_transform(const cv::Mat &img, std::vector<cv::Point2f> pts, double aspect_ratio);

void matwrite(const std::string &filename, const cv::Mat &mat);

cv::Mat matread(const std::string &filename);

bool IsDateValid(int day, int month, int year);

/**
 * @param year - should be in a four digit dormat
 */
bool IsDateValid(std::string day, std::string month, std::string year);
} // namespace vdoc

#endif // VOCR_VOCR_UTIL_H
