#pragma once

#include <fstream>
#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include "document_meta.h"
#include "utils_mrz.h"
#include "visual_document.h"
#include "visual_document_factory.h"
#include <vocr/blur_detector.h>
#include <vocr/classifier_chars.h>
#include <vocr/doc_type.h>
#include <vocr/exception/invalid_argument_exception.h>
#include <vocr/post_processing.h>
#include <vocr/utils.h>
#include <vocr/utils_iin.h>
#include <vocr/wordinator_interface.h>

namespace vdoc {

/**
 * Creates a incomplete VisualDocument with empty original image, but with one 'TextField' with many 'Lines'
 * @param lines
 * @param name
 * @param type
 * @return
 */
std::shared_ptr<VisualDocument> createVisualDocForTextField(const std::vector<cv::Mat> &lines, const std::string &name,
                                                            int type);

/**
 * Returns the estimated number of symbols in the text line.
 * @param line - Image of a text line
 * @return Number of symbols
 */
int countSymbols(const cv::Mat &line);

/**
 * Returns the estimated number of symbols in the text line.
 * @param visdoc_line - VisualDocument for a text line
 * @return Number of symbols
 */
int countSymbols(std::shared_ptr<VisualDocument> &visdoc_line);

/**
 * Performs OCR on provided images as if each of them contains only one line of text. It creates VisualDocument where
 * only one TextField with many lines.
 * @param lines - Images of lines to recognize
 * @param labels - name of a text field
 * @return VisualDocument with one TextField
 */
std::shared_ptr<vdoc::VisualDocument> extractField(const std::vector<cv::Mat> &lines, const std::string &name,
                                                   const int &type);

/**
 *
 * @param visdoc
 */
void extract(std::shared_ptr<VisualDocument> &vis_doc, bool detect_text, bool predict);

/**
 * Convenience function for calling the SmartGrabber
 * @param src
 * @param dtype
 * @param atype
 * @return
 */
static cv::Mat grabSmartly(const cv::Mat &src, DocType &dtype);

/**
 * Convenience function for calling the GrabberMrz
 * @param src
 * @return
 */
std::shared_ptr<vdoc::VisualDocument> grabMrz(const cv::Mat &src, vdoc::mrz::MrzType &typ);

/**
 * Performs text recognition on the provided image. It expects that the actual type of the document on the image
 * passed to the method matches to the 'type' (second parameter) variable.
 *
 * @param doc_img An image to be processed.
 * @param doc_type Type of the document be recognized
 * @return Returns abstract Document which contains all the extracted texts.
 *         Perform casting to the exact type of the recognized document the recognition is finished
 */
std::shared_ptr<vdoc::VisualDocument> extract(const cv::Mat &doc_img, const DocType &doc_type);

std::shared_ptr<vdoc::VisualDocument> extractWithoutGrabber(const cv::Mat &doc_img, const DocType &doc_type);
/**
 * Performs MRZ recognition on the provided image.
 *
 * @param doc_img An image to be processed.
 * @return Returns abstract Document which contains all the extracted texts.
 *         Perform casting to the exact type of the recognized document the recognition is finished
 */
std::shared_ptr<vdoc::VisualDocument> extractMrz(const cv::Mat &doc_img);

/**
 * Performs text recognition on the provided sequence of images. It expects that the actual type of the document on
 * the image passed to the method matches to the 'type' (second parameter) variable.
 *
 * @param doc_imgs Images to be processed.
 * @param doc_type Type of the document be recognized
 * @return Returns abstract Document which contains all the extracted texts.
 *         Perform casting to the exact type of the recognized document the recognition is finished
 */
std::shared_ptr<vdoc::VisualDocument> sequenceExtract(std::vector<cv::Mat> doc_imgs, vdoc::DocType doc_type);

/**
 * @brief Performs MRZ recognition on the provided sequence of images (stream).
 *
 * @param doc_imgs sequence of images
 * @return std::shared_ptr<vdoc::VisualDocument> Returns abstract Document which contains all the extracted texts.
 */
std::shared_ptr<vdoc::VisualDocument> sequenceExtractMrz(std::vector<cv::Mat> doc_imgs);

/**
 * Predict document type. Used mainly in quality test.
 * (TODO: Add some macro flag - so that it only exists for quality test (not in release build))
 */
// vdoc::DocType predictDocType(cv::Mat doc_img);

/**
 * @brief Predicts MRZ type. Used mainly in quality test.
 * (TODO: Add some macro flag - so that it only exists for quality test (not in release build))
 *
 * @param doc_img image
 * @return vdoc::mrz::MrzType MRZ type
 */
vdoc::mrz::MrzType predictMrzType(const cv::Mat &doc_img);

/**
 * An instance of PostProcessing class used to process raw kernel results
 */
static vdoc::PostProcessing post_processing;

std::string visDocToJson(const std::shared_ptr<vdoc::VisualDocument> &vis_doc, bool extract_images);

/**
 * Performs document recognition on the provided image.
 *
 * @param image_path path to image to be processed
 * @param doc_type Type of the document be recognized
 * @param extract_images whether to extract image fields as well, or only text fields
 * @return Returns JSON result string (images are base64-encoded jpeg files).
 */
std::string extractJson(std::string &image_path, vdoc::DocType doc_type, bool extract_images = true);
std::string extractJsonWithoutGrabbing(std::string &image_path, vdoc::DocType doc_type, bool extract_images = true);
/**
 * Performs document recognition on the provided image.
 *
 * @param img image to be recognized
 * @param doc_type Type of the document be recognized
 * @param extract_images whether to extract image fields as well, or only text fields
 * @return Returns JSON result string (images are base64-encoded jpeg files).
 */
std::string extractJson(cv::Mat &img, vdoc::DocType doc_type, bool extract_images = true);

std::string extractJsonWithoutGrabbing(cv::Mat &img, vdoc::DocType doc_type, bool extract_images = true);

/**
 * Performs MRZ document recognition on the provided image.
 *
 * @param imgs Images to be recognized.
 * @return Returns JSON result string.
 */
std::string extractMrzJson(cv::Mat &img);
std::string extractMrzJson(std::string &image_path);

/**
 * @brief Performs document recognition on the sequence of images (stream).
 *
 * @param imgs sequence of images
 * @param doc_type Document Type
 * @param extract_images whether to extract image fields as well, or only text fields
 * @return std::string Returns JSON result string (images are base64-encoded jpeg files).
 */
std::string sequenceExtractJson(std::vector<cv::Mat> imgs, vdoc::DocType doc_type, bool extract_images = true);

/**
 * @brief Performs MRZ document recognition on the sequence of images (stream).
 *
 * @param imgs sequence of images
 * @return std::string Returns JSON result string.
 */
std::string sequenceExtractMrzJson(std::vector<cv::Mat> imgs);

/**
 * @brief Returns results of an autocapturing attempt of MRZ image in JSON string format.
 *
 * @param img image
 * @return std::string JSON string format:
 *         {
 *           "found": true, // If found MRZ in the img
 *           "lines": [ // List of coordinates of mrz lines (max 3 lines)
 *                     { // Coordinates of one line
 *                       "tl": {"x": 56, "y": 227},   // Top-left
 *                       "tr": {"x": 446, "y": 225},  // Top-right
 *                       "br": {"x": 56, "y": 211},   // Bottom-right
 *                       "bl": {"x": 445, "y": 209}   // Bottom-left
 *                     },
 *                     ...
 *                    ]
 *         }
 */
// std::string nextImageMrz(cv::Mat &img);

/**
 * @brief Predicts whether given image is a corner
 *
 * @param img Image to predict
 * @return int 0 - no corner, 1 - corner
 */
int isCorner(cv::Mat &img);

} // namespace vdoc
