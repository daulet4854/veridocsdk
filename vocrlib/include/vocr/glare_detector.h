#ifndef VOCR_GLARE_DETECTOR_H
#define VOCR_GLARE_DETECTOR_H

#include <memory>
#include <vector>

#include <opencv2/core/mat.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include "../../src/visual_document/field.h"
#include "visual_document.h"
#include <vocr/interface.h>

namespace vdoc {
class GlareDetector {
  public:
    std::vector<cv::Rect> localize(cv::InputArray _src, int _min_lum = 253, int _max_lum = 255);
    bool isDocumentApplicable(cv::Mat &src, std::shared_ptr<vdoc::VisualDocument> doc, std::vector<cv::Rect> contours);
};
} // namespace vdoc

#endif // VOCR_GLARE_DETECTOR_H