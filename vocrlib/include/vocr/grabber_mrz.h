//
// Created by chingy on 8/18/18.
//

#ifndef VOCR_GRABBER_MRZ_H
#define VOCR_GRABBER_MRZ_H

#include "grabber.h"
#include "utils_mrz.h"

namespace vdoc {
namespace grab {

class GrabberMrz : public Grabber {
  private:
    /**
     * kernel for blackhat morphological operation which finds dark regions against light background
     */
    const cv::Mat kernel_for_blackhat_ = cv::getStructuringElement(cv::MORPH_RECT, {5, 3});

    /**
     * Kernel for closing morphological operation
     */
    const std::vector<cv::Mat> kernel_for_closeop_ = {cv::getStructuringElement(cv::MORPH_RECT, {15, 3}),
                                                      cv::getStructuringElement(cv::MORPH_RECT, {11, 3})};

    /**
     * aspect ratio of the bounding boxes of mrz lines, min
     */
    const double line_ar_min_ = 0.02;

    /**
     * aspect ratio of the bounding boxes of mrz lines, max
     */
    const double line_ar_max_ = 0.11;

    /**
     * Min width of the mrz line relative to the image width
     */
    const float min_width_ = 0.4;

    /**
     * Min left wnd right padding for an mrz line (0.01*width- for the left side, 0.99*width - for the right side)
     */
    const float side_padding_ = 0.01;

    /**
     * Max allowed difference between widths of MRZ lines
     */
    const float max_diff_ = 0.04;

    /**
     * Min horizontal displacement of the mrz lines
     */
    const float max_shift_ = 0.05;

    /**
     * MRZ type according to standards
     */
    vdoc::mrz::MrzType mrz_type_ = vdoc::mrz::MrzType::TD1;

  public:
    bool isSuccess() override;

    void prepareVisualDocument() override;

    bool process() override;

    bool isSame(std::shared_ptr<GrabbingResult> &res1, std::shared_ptr<GrabbingResult> &res2) override;

    vdoc::mrz::MrzType getMrzType() const;

    GrabberMrz() { type_ = DocType::MRZ; }

  private:
    int getProcessingWidth() override;

    /**
     * Finds MRZ lines
     * @param img
     * @return
     */
    std::shared_ptr<GrabbingResult> findMrzLines(cv::Mat &img);

    /**
     * Adds some margins to the region, taking into account the angle of the rotated rectangle
     * @param corners
     * @param angle
     */
    void addMargins(std::vector<cv::Point2f> &corners, const double &angle);

    std::shared_ptr<GrabbingResult> processImage(cv::Mat &src);

    std::vector<cv::Mat> generateImages();

    std::vector<cv::Mat> crop(const cv::Mat &img);
};
} // namespace grab
} // namespace vdoc

#endif // VOCR_GRABBER_MRZ_H
