//
// Created by chingy on 8/2/18.
//

#ifndef VOCR_DOCUMENT_NOT_FOUND_EXCEPTION_H
#define VOCR_DOCUMENT_NOT_FOUND_EXCEPTION_H

#include <exception>
#include <string>

namespace vdoc {
class DocumentNotFoundException : public std::exception {
  private:
    std::string message_ = "No document found on provided image(s).";

  public:
    explicit DocumentNotFoundException() {}

    explicit DocumentNotFoundException(std::string message) : message_(message) {}

    virtual const char *what() const throw() { return message_.c_str(); }
};
} // namespace vdoc

#endif // VOCR_DOCUMENT_NOT_FOUND_EXCEPTION_H
