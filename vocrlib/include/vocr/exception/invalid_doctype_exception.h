#ifndef VERIDOC_KERNEL_INVALIDDOCTYPEEXCEPTION_H
#define VERIDOC_KERNEL_INVALIDDOCTYPEEXCEPTION_H

#include <exception>
#include <string>

namespace vdoc {
class InvalidDoctypeException : public std::exception {
  private:
    std::string message_ = "Invalid Document type.";

  public:
    explicit InvalidDoctypeException(std::string message) : message_(message) {}

    virtual const char *what() const throw() { return message_.c_str(); }
};
} // namespace vdoc

#endif // VERIDOC_KERNEL_INVALIDDOCTYPEEXCEPTION_H
