//
// Created by chingy on 10/10/17.
//

#ifndef VERIDOC_KERNEL_INVALIDARGUMENTEXCEPTION_H
#define VERIDOC_KERNEL_INVALIDARGUMENTEXCEPTION_H

#include <exception>
#include <string>
#include <vector>

namespace vdoc {

class InvalidArgumentException : public std::exception {
  private:
    std::string message_ = "Invalid argument is passed";

  public:
    explicit InvalidArgumentException(std::string message) : message_(message) {}

    virtual const char *what() const throw() { return message_.c_str(); }
};
} // namespace vdoc

#endif // VERIDOC_KERNEL_INVALIDARGUMENTEXCEPTION_H
