#ifndef VERIDOC_KERNEL_IMAGE_TOO_BLURED_EXCEPTION_H
#define VERIDOC_KERNEL_IMAGE_TOO_BLURED_EXCEPTION_H

#include <exception>
#include <string>

namespace vdoc {
class BlurryImageException : public std::exception {
  private:
    std::string message_ = "Images are too blured";

  public:
    explicit BlurryImageException(std::string message) : message_(message) {}

    virtual const char *what() const throw() { return message_.c_str(); }
};
} // namespace vdoc

#endif // VERIDOC_KERNEL_IMAGE_TOO_BLURED_EXCEPTION_H
