//
// Created by chingy on 12/15/17.
//

#ifndef VOCR_IMAGE_OF_FIELD_NOT_FOUND_EXCEPTION_H
#define VOCR_IMAGE_OF_FIELD_NOT_FOUND_EXCEPTION_H

#include <exception>

namespace vdoc {

class ImageOfFieldNotFoundException : public std::exception {
  private:
    std::string message_ = "Could not locate field";

  public:
    explicit ImageOfFieldNotFoundException(std::string message) : message_(message) {}

    virtual const char *what() const throw() { return message_.c_str(); }
};
} // namespace vdoc
#endif // VOCR_IMAGE_OF_FIELD_NOT_FOUND_EXCEPTION_H
