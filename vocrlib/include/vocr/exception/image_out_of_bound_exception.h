//
// Created by chingy on 11/8/17.
//

#ifndef VOCR_IMAGE_OUT_OF_BOUND_EXCEPTION_H
#define VOCR_IMAGE_OUT_OF_BOUND_EXCEPTION_H

#include <exception>

namespace vdoc {

class ImageOutOfBoundException : public std::exception {
    virtual const char *what() const throw() { return "Some parameters exceed image dimensions"; }
};
} // namespace vdoc

#endif // VOCR_IMAGE_OUT_OF_BOUND_EXCEPTION_H
