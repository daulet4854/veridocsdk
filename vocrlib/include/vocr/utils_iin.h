#ifndef VOCR_IINUTIL_H
#define VOCR_IINUTIL_H

#include <algorithm>
#include <string>

namespace vdoc {
namespace iin {

/**
 * Given IIN string, it will perform full IIN validation.
 * (CheckDigit, CheckDate, length, all digits)
 *
 * @param iin, IIN string.
 * @return true, if IIN is valid,
 *         false, otherwise.
 */
bool FullVerify(const std::string &iin);

/**
 * Given IIN string, it will perform check digit verification.
 *
 * @param iin, IIN string.
 * @return true, if check digits is valid,
 *         false, otherwise.
 */
bool CheckDigitVerify(const std::string &iin);

// SHOULD BE IN DATE FORMATTER?
/**
 * Given date substring from MRZ/IIN formats, it will perform date validity check.
 *
 * @param date_str date string in MRZ/IIN format - "YYMMDD".
 * @return true, if date_str is valid,
 *         false, otherwise.
 */
bool CheckDate(const std::string &date_str);

/**
 * Given IIN string, it will return gender.
 *
 * @param iin, IIN string.
 * @return true, if check digits is valid,
 *         false, otherwise.
 */
std::string GetGender(const std::string &iin);

/**
 * Given IIN string, it will return birth year hundreds (a.k.a Century - 1).
 * E.g. if 20th centuary, return "19"
 *
 * @param iin, IIN string.
 * @return true, if check digits is valid,
 *         false, otherwise.
 */
std::string GetBirthYearHundreds(const std::string &iin);

/**
 * Given IIN string, it will return gender.
 *
 * @param iin, IIN string.
 * @return true, if check digits is valid,
 *         false, otherwise.
 */
std::string GetFullBirthDate(const std::string &iin);

} // namespace iin
} // namespace vdoc

#endif
