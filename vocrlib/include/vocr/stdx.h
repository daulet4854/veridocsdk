#ifndef VOCR_STDX_H
#define VOCR_STDX_H

#include <functional>
#include <memory>
#include <sstream>
#include <tuple>
#include <utility>

namespace stdx {

/**
 * std lib generic to_string conversion extension
 */
template <typename T> std::string to_string(const T &arg) {
    std::ostringstream stream;
    stream << arg;
    return stream.str();
}

/**
 * takes a variable number of string arguments and stdx::concatenates them into a single string.
 */
template <typename... Args> std::string concat(Args const &... args) {
    std::ostringstream stream;
    using List = int[];
    (void)List{0, ((void)(stream << args), 0)...};

    return stream.str();
}

template <typename T, typename U> inline bool equals(const T &lhs, const U &rhs) {
    return (lhs && rhs) ? *lhs == *rhs : !lhs && !rhs;
}

template <class T, class U> inline bool equals(const std::shared_ptr<T> &lhs, const std::shared_ptr<U> &rhs) {
    return lhs == rhs && lhs.get() == rhs.get();
}

template <typename Base, typename T> inline bool is_base_of(const T *) { return std::is_base_of<Base, T>::value; }

template <typename S, typename T> inline bool instanceof (const T &instance) {
    return (dynamic_cast<S *>(instance) != NULL);
}

/**
 *  a parameter pack used to generate a type seq<0, 1, .., N-1>
 */
template <int...> struct seq {};

template <int N, int... S> struct gens : gens<N - 1, N - 1, S...> {};

template <int... S> struct gens<0, S...> { typedef seq<S...> type; };

/**
 * recursively evaluates function arguments via unpacking operator required
 * for a function call and dispatches the event (function/lambda expression)
 */
template <typename F, typename Tuple, int... S> void apply(F &&foo, Tuple &&params, seq<S...>) {
    foo(std::get<S>(std::forward<Tuple>(params))...);
}

template <typename F, typename Tuple> void apply(F foo, Tuple params) {
    apply(std::forward<F>(foo), std::forward<Tuple>(params), typename gens<std::tuple_size<Tuple>::value>::type());
}
} // namespace stdx

#endif // VOCR_STDX_H