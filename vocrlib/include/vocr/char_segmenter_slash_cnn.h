//
// Created by chingy on 2/8/19.
//

#ifndef VOCR_CHAR_SEGMENTER_SLASH_CNN_H
#define VOCR_CHAR_SEGMENTER_SLASH_CNN_H

#include "char_segmenter_cnn.h"
#include "char_segmenter_cnn_models.h"

namespace vdoc {
class CharSegmenterSlashCnn : public CharSegmenterCnn {
  public:
    CharSegmenterSlashCnn() {
        setModel(std::make_shared<CharSegmenterSlashCnnModel>());
        setThreshold(0.92);
    }

    void setSegmentsMinWidth(int thresh_min_width);

    std::vector<cv::Rect2i> segment(const cv::Mat &txt_line_img) override;

  private:
    std::vector<cv::Rect2i> filterByMinWidth(const std::vector<cv::Rect2i> &segments) const;

    int min_segment_width_ = 3;
};
} // namespace vdoc
#endif // VOCR_CHAR_SEGMENTER_SLASH_CNN_H
