//
// Created by chingy on 1/31/19.
//

#ifndef VOCR_DNN_MODEL_H
#define VOCR_DNN_MODEL_H

#include "dnn_model_interface.h"
#include <opencv2/dnn.hpp>

namespace vdoc {

class AbstractDnnModel : public DnnModelInterface {
  public:
    cv::Mat predict(const cv::Mat &src_image) override;

    cv::Mat predict(const std::vector<cv::Mat> &src_images) override;

  private:
    virtual cv::dnn::Net network() = 0;
    virtual double scaleValue();

    virtual void setImages(const std::vector<cv::Mat> &images);
    virtual bool validate();

    cv::Mat forward(const cv::Mat &blob);

    std::vector<cv::Mat> images_;
};
} // namespace vdoc

#endif // VOCR_DNN_H
