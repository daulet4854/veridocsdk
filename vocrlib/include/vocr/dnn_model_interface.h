//
// Created by chingy on 1/31/19.
//

#ifndef VOCR_DNN_MODEL_INTERFACE_H
#define VOCR_DNN_MODEL_INTERFACE_H

#include <opencv2/core/mat.hpp>

namespace vdoc {

class DnnModelInterface {
  public:
    virtual cv::Mat predict(const cv::Mat &src_image) = 0;
    virtual cv::Mat predict(const std::vector<cv::Mat> &src_images) = 0;
    virtual cv::Size inputSize() = 0;
    virtual int inputChannels() = 0;
    virtual ~DnnModelInterface() {}
};
} // namespace vdoc
#endif // VOCR_DNN_H
