//
// Created by chingy on 10/16/17.
//

#ifndef KERNEL_OCR_WORDINATOR_INTERFACE_H
#define KERNEL_OCR_WORDINATOR_INTERFACE_H

#include "visual_document.h"
#include <memory>
#include <opencv2/core/mat.hpp>
#include <string>
#include <vector>
#include <vocr/classifier_interface.h>
#include <vocr/doc_type.h>

namespace vdoc {

/**
 * This class is responsible for splitting images of blobs of texts into words, and also for orchestration of
 * classes that splits words into individual symbols and recognizes them.
 *
 * It receives a vector of images that presumably contains blobs of text (lines extracted from documents)
 * Returns a vector of vector of strings  - recognized words per each image in the input vector.
 */
class VocrWordinatorInterface {
  public:
    static std::shared_ptr<VocrWordinatorInterface> make_wordinator(const DocType &type);

    virtual void segmentChars(std::shared_ptr<ImageView> &field) = 0;

    virtual std::tuple<int, std::string, double> classifyChars(std::shared_ptr<ImageView> &field,
                                                               std::shared_ptr<ClassifierInterface> &classifier) = 0;

    virtual ~VocrWordinatorInterface() = default;
};

} // namespace vdoc
#endif // KERNEL_OCR_WORDINATOR_INTERFACE_H
