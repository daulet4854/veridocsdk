//
// Created by chingy on 2/2/19.
//

#ifndef VOCR_DIGITS_CNN_SEGMENTER_H
#define VOCR_DIGITS_CNN_SEGMENTER_H

#include "char_segmenter_interface.h"
#include "dnn_model_interface.h"
#include <memory>

namespace vdoc {
class CharSegmenterCnn : public CharSegmenterInterface {
  public:
    /**
     * Segments provided text line. Main method
     * @param txt_line_img
     * @return
     */
    std::vector<cv::Rect2i> segment(const cv::Mat &txt_line_img) override;

    /**
     * Sets the character segmentation CNN model
     * @param model
     */
    void setModel(const std::shared_ptr<DnnModelInterface> &model);

    /**
     * Sets values for left and right margins of each detected character segment
     * @param left
     * @param right
     */
    void setMargins(int left, int right);

    /**
     * Threshold for probabilities
     * @param thresh
     */
    void setThreshold(float thresh);

    cv::Size referenceSize() override;

  protected:
    virtual std::vector<float> processProbabilities(const std::vector<float> &src_probs);

  private:
    std::shared_ptr<DnnModelInterface> model_;

    int left_margin_ = 0;
    int right_margin_ = 0;
    float thresh_ = 0.5f;

    float scale_ = 1.0f;

    std::vector<float> mat2vector(const cv::Mat &mat);
    std::vector<std::pair<int, int>> decode(const std::vector<bool> &hot_encoded);
    std::vector<bool> threshold(const std::vector<float> &cnn_out);
    std::vector<std::pair<int, int>> addMargins(const std::vector<std::pair<int, int>> &segments, int img_width);
    std::vector<cv::Rect2i> prepareRectSegments(std::vector<std::pair<int, int>> segments, int img_height);

    std::vector<std::pair<int, int>> scaleBack(const std::vector<std::pair<int, int>> &segments, int img_width);
    cv::Mat prepareImage(const cv::Mat &src_image);
    cv::Mat shrinkOrPadAlongWidth(const cv::Mat &input, int width);
};
} // namespace vdoc
#endif // VOCR_DIGITS_SEGMENTER_CNN_H
