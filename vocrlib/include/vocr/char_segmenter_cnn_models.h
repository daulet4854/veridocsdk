//
// Created by chingy on 2/4/19.
//

#ifndef VOCR_DIGITS_CNN_SEGMENTER_MODEL_H
#define VOCR_DIGITS_CNN_SEGMENTER_MODEL_H

#include "dnn_model.h"
#include <opencv2/dnn.hpp>

namespace vdoc {
class CharSegmenterDigitsCnnModel : public AbstractDnnModel {
  public:
    cv::Size inputSize() override { return {width_, height_}; }
    int inputChannels() override { return channels_; }

  private:
    cv::dnn::Net network() override { return model_; };

    /**
     * Model that segments digits only
     */
    static cv::dnn::Net model_;

    const int width_ = 100;
    const int height_ = 16;
    const int channels_ = 1;
};

class CharSegmenterSlashCnnModel : public AbstractDnnModel {
  public:
    cv::Size inputSize() override { return {width_, height_}; }

    int inputChannels() override { return channels_; }

  private:
    cv::dnn::Net network() override { return model_; }

    /**
     * Model that segments right slashes only
     */
    static cv::dnn::Net model_;

    const int width_ = 80;
    const int height_ = 16;
    const int channels_ = 1;
};

} // namespace vdoc

#endif // VOCR_DIGITS_CNN_SEGMENTER_MODEL_H
