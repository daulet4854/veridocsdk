//
// Created by chingy on 9/7/18.
//

#ifndef VOCR_OCR_PARAMS_H
#define VOCR_OCR_PARAMS_H

namespace vdoc {
/**
 * The structure that contains all necessary parameters for OCR
 */
struct OcrParams {

    /**
     * Defines whether font of the line is monospace. When true max/min digit/letter widths must be equal.
     * json entry name : is_monospace
     */
    bool is_monospace = false;

    /**
     * Defines whether view is placed incide the tabular structure.
     * json entry name : is_tabular
     */
    bool is_tabular = false;

    /**
     * Minimum possible width of the letter in the line (for predefined processing height). (pixels)
     * json entry name : letter_min_width
     */
    int letter_min_width = 12;

    /**
     * Maximum possible width of the letter in the line (for predefined processing height). (pixels)
     * json entry name : letter_max_width
     */
    int letter_max_width = 45;

    /**
     * Average width of the letter in the line (for predefined processing height). (pixels)
     * json entry name : letter_avg_width
     */
    int letter_avg_width = 24;

    /**
     * Space between the letters in the line (for predefined processing height). (pixels)
     * json entry name : letter_space
     */
    int letter_space = 7;

    /**
     * Minimum possible width of the digit in the line (for predefined processing height). (pixels)
     * json entry name : digit_min_width
     */
    int digit_min_width = 13;

    /**
     * Maximum possible width of the digit in the line (for predefined processing height). (pixels)
     * json entry name : digit_max_width
     */
    int digit_max_width = 35;

    /**
     * Average width of the digits in the line (for predefined processing height). (pixels)
     * json entry name : digit_avg_width
     */
    int digit_avg_width = 19;

    /**
     * Space between the digits in the line (for predefined processing height). (pixels)
     * json entry name : digit_space
     */
    int digit_space = 5;

    /**
     * Min width of a space between words, in pixels
     * json entry name : white_space
     */
    int white_space = 20;

    /**
     * Min height of a character in an image of a text line or word.
     * Contours below this value considered as a trash.
     * json entry name : char_min_height
     */
    int char_min_height = 22;

    /**
     * Min height of a text line in the image that conatains many lines of text.
     * Everything below this value is not considered as a text for recognition.
     * json entry name : textline_min_height
     */
    int textline_min_height = 0;

    /**
     * The height of an image to which all word images are resized, keeping aspect ratio, before processing.
     * json entry name : processing_height
     */
    int processing_height = 45;
};
} // namespace vdoc

#endif // VOCR_OCR_PARAMS_H
