//
// Created by chingy on 7/30/18.
//

#ifndef VOCR_GRABBER_BORDERS_BASED_H
#define VOCR_GRABBER_BORDERS_BASED_H

#include "grabber.h"

namespace vdoc {
namespace grab {
class GrabberBorders : public grab::Grabber {

  private:
    void cropLeft(cv::Mat &dst_crop, cv::Point2i &offset);
    void cropTop(cv::Mat &dst_crop, cv::Point2i &offset);
    void cropRight(cv::Mat &dst_crop, cv::Point2i &offset);
    void cropBottom(cv::Mat &dst_crop, cv::Point2i &offset);

    /**
     *
     * @param roi
     * @param offset
     * @param hor - indicates whether to look only for horizonatl lines, if False then find only vertical lines
     */
    void findLines(const short &label, const cv::Mat &roi, const cv::Point2i &offset, const bool &hor);

    /**
     * Returns lines from the 'result_' that have specific label
     * @param label
     * @return
     */
    std::vector<Line> getLines(const short &label);

    /**
     * Calsulates intersection points of 2 given set of lines
     * @param lines1 - first set of lines
     * @param lines2 - second set of lines
     * @return Collection of Points
     */
    std::vector<cv::Point> calculateIntersections(const std::vector<Line> &lines1, const std::vector<Line> &lines2);

  protected:
    void prepareVisualDocument() override;

  protected:
    bool isSuccess() override;

  public:
    bool process() override;

    bool isSame(std::shared_ptr<GrabbingResult> &res1, std::shared_ptr<GrabbingResult> &res2) override;
};
} // namespace grab
} // namespace vdoc

#endif // VOCR_GRABBER_BORDERS_BASED_H
