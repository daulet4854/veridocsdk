//
// Created by chingy on 2/15/19.
//

#ifndef VOCR_CHAR_SEGMENTER_DIGITS_BASIC_H
#define VOCR_CHAR_SEGMENTER_DIGITS_BASIC_H

#include "char_segmenter_interface.h"
#include "image_proc_interface.h"
#include "ocr_params.h"
#include "utils_contours.h"
#include <memory>
#include <opencv2/imgproc.hpp>

namespace vdoc {

class CharSegmenterDigitsBasic : public CharSegmenterInterface {
  public:
    explicit CharSegmenterDigitsBasic(const OcrParams &params);

    std::vector<cv::Rect2i> segment(const cv::Mat &txt_line_img) override;

    cv::Size referenceSize() override;

    void setCharDetector(const std::shared_ptr<ImageProcessorInterface> &processor);

  protected:
    std::shared_ptr<ImageProcessorInterface> char_detector_;
    OcrParams ocr_params_;

    bool allCharBoxesWidthLessThanMax(std::vector<cv::Rect> &char_boxes) const;
    void addMargins(std::vector<cv::Rect> &boxes, int left, int right);
    void embraceWholeImageHeight(int height, std::vector<cv::Rect> &char_boxes) const;
};

class SimpleCharDetector : public ImageProcessorInterface {
  public:
    explicit SimpleCharDetector(int min_height, int min_width, int gauss_thresh_kernel)
        : min_height_(min_height), min_width_(min_width), gauss_thresh_kernel_(gauss_thresh_kernel) {}

    void process(const cv::Mat &src) override;

    cv::Mat processed() override;

    std::vector<cv::Rect> rois() override;

    std::vector<cv::RotatedRect> fineRois() override;

  protected:
    int min_height_ = 0;
    int min_width_ = 0;
    int gauss_thresh_kernel_ = 0;
    std::vector<cv::Rect> rois_;

    void charThreshold(const cv::Mat &in, cv::Mat &out, int c = 1);
    void removeBorderingWhitePixels(cv::Mat &out) const;
    std::vector<cv::Rect> findCharsBoundingBoxes(const cv::Mat &thresholded) const;
    std::vector<cv::Rect> detectAndOrderChars(const cv::Mat &src, int c);
};

} // namespace vdoc

#endif // VOCR_CHAR_SEGMENTER_DIGITS_BASIC_H
