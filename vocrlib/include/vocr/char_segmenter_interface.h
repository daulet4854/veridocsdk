//
// Created by chingy on 2/1/19.
//

#ifndef VOCR_CHAR_SEGMENTER_INTERFACE_H
#define VOCR_CHAR_SEGMENTER_INTERFACE_H

#include <opencv2/core/types.hpp>

namespace vdoc {
class CharSegmenterInterface {
  public:
    virtual std::vector<cv::Rect2i> segment(const cv::Mat &txt_line_img) = 0;
    virtual cv::Size referenceSize() = 0;

    ~CharSegmenterInterface() = default;
};
} // namespace vdoc
#endif // VOCR_CHAR_SEGMENTER_INTERFACE_H
