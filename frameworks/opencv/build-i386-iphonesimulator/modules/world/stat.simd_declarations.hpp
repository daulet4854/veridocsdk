#define CV_CPU_SIMD_FILENAME "/Users/chingy/Dev/opencv/opencv-4.1.1/modules/core/src/stat.simd.hpp"
#define CV_CPU_DISPATCH_MODE SSE4_2
#include "opencv2/core/private/cv_cpu_include_simd_declarations.hpp"

#define CV_CPU_DISPATCH_MODES_ALL SSE4_2, BASELINE

#undef CV_CPU_SIMD_FILENAME
