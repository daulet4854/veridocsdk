# CMake generated Testfile for 
# Source directory: /Users/chingy/Dev/opencv/opencv-4.1.1
# Build directory: /Users/chingy/Dev/opencv/ios/build/build-armv7s-iphoneos
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("3rdparty/zlib")
subdirs("3rdparty/libjpeg-turbo")
subdirs("3rdparty/libwebp")
subdirs("3rdparty/libpng")
subdirs("3rdparty/protobuf")
subdirs("3rdparty/quirc")
subdirs("include")
subdirs("modules")
subdirs("doc")
subdirs("data")
