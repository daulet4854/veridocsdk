
#define OPENCV_INSTALL_PREFIX "/Users/chingy/Dev/opencv/ios/build/build-armv7s-iphoneos/install"

#define OPENCV_DATA_INSTALL_PATH "share/opencv4"

#define OPENCV_BUILD_DIR "/Users/chingy/Dev/opencv/ios/build/build-armv7s-iphoneos"

#define OPENCV_DATA_BUILD_DIR_SEARCH_PATHS \
    "../../../opencv-4.1.1/"

#define OPENCV_INSTALL_DATA_DIR_RELATIVE "../share/opencv4"
