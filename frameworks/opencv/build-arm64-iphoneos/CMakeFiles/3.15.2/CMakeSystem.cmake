set(CMAKE_HOST_SYSTEM "Darwin-18.7.0")
set(CMAKE_HOST_SYSTEM_NAME "Darwin")
set(CMAKE_HOST_SYSTEM_VERSION "18.7.0")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/Users/chingy/Dev/opencv/opencv-4.1.1/platforms/ios/cmake/Toolchains/Toolchain-iPhoneOS_Xcode.cmake")

set(CMAKE_SYSTEM "iOS-8.0")
set(CMAKE_SYSTEM_NAME "iOS")
set(CMAKE_SYSTEM_VERSION "8.0")
set(CMAKE_SYSTEM_PROCESSOR "arm64")

set(CMAKE_CROSSCOMPILING "TRUE")

set(CMAKE_SYSTEM_LOADED 1)
