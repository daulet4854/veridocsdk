#import "ScannerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <GLKit/GLKit.h>
#import <CoreImage/CoreImage.h>
#import <CoreGraphics/CGPath.h>
#import "UIImage+Veridoc.h"
#import "ScannerMaskView.h"
#import "GeometryUtil.h"
#include <vocr/grabber.h>
#include <vocr/grabber_borders.h>
#include <vocr/grabber_smart.h>
#include <vocr/utils_id.h>
#include <vocr/exception/blurry_image_exception.h>
#include <vocr/exception/document_not_found_exception.h>

#define ID_ASPECT_RATIO 1.586

@interface ScannerViewController ()<AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic) AVCaptureSession *captureSession;
@property (nonatomic) AVCaptureDeviceInput *input;
@property (nonatomic) AVCaptureVideoDataOutput *videoDataOutput;
@property (nonatomic) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic) AVCaptureDevice *backCamera;
@property (nonatomic, strong) UIView *previewView;
@property (nonatomic, strong) ScannerMaskView *whiteView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UIImageView *statusImageView;

@property (nonatomic) std::shared_ptr<vdoc::VideoAdapter> video_adapter;
@property (nonatomic) std::shared_ptr<vdoc::VisualDocument> front_vis_doc;
@property (nonatomic) std::shared_ptr<vdoc::VisualDocument> back_vis_doc;
@property (nonatomic, assign) int defaultFPS;
@property (nonatomic, assign) BOOL grayscaleMode;
@property (nonatomic, assign) int currentStep;
@property (nonatomic, strong) NSDictionary *frontSideResult;
@property (nonatomic, strong) NSDictionary *backSideResult;
@property (nonatomic, strong) NSString *const defaultAVCaptureSessionPreset;
@property (nonatomic, assign) AVCaptureVideoOrientation defaultAVCaptureVideoOrientation;
@property (nonatomic) dispatch_queue_t sessionQueue;

@end

@implementation ScannerViewController {
    dispatch_queue_t videoDataOutputQueue;
    float width_ratio;
    float height_ratio;
    int croppedWidth;
    int croppedHeight;
}

- (void)dealloc {

}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBarHidden = YES;

    // set default values
    self.defaultFPS = 30;
    self.grayscaleMode = NO;
    self.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.currentStep = 1;  // 1 - for front side, 2 - for back side;
    self.frontSideResult = [NSDictionary dictionary];
    self.backSideResult = [NSDictionary dictionary];

    switch (self.type) {
        case IdentityDocument:
            self.video_adapter = std::make_shared<vdoc::VideoAdapter>(vdoc::DocType::KZ_ID, false, true);
            break;
        case MRZ:
            self.video_adapter = std::make_shared<vdoc::VideoAdapter>(vdoc::DocType::MRZ, false, true);
            break;
        case BeelineSimCard:
            self.video_adapter = std::make_shared<vdoc::VideoAdapter>(vdoc::DocType::BEELINE_SIMCARD, false, true);
            break;
        default:
            break;
    }
    
    CGRect cardFrame = [self offsetRectWithRect: self.view.frame];
    CGFloat cardX = CGRectGetMinX(cardFrame);
    CGFloat cardY = CGRectGetMinY(cardFrame);
    CGFloat cardWidth = CGRectGetWidth(cardFrame);
    CGFloat cardHeight = CGRectGetHeight(cardFrame);
    
    // preview
    self.previewView = [UIView new];
    [self.view addSubview:self.previewView];
    
    // whiteView
    self.whiteView = [ScannerMaskView new];
    self.whiteView.backgroundColor = [UIColor clearColor];
    self.whiteView.frame = self.view.frame;
    [self.view addSubview: self.whiteView];
    
    // close button
    NSBundle *veridocBundle = [NSBundle bundleForClass:[self class]];
    UIImage *closeIcon = [UIImage imageNamed:@"close_white" inBundle:veridocBundle compatibleWithTraitCollection:nil];
        
    self.closeButton = [[UIButton alloc] initWithFrame:CGRectZero];
    self.closeButton.hidden = YES;
    [self.closeButton setImage:closeIcon forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(suspend:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.closeButton];
    [self.view layoutIfNeeded];
    
    // title label
    
    self.titleLabel = [UILabel new];
    self.titleLabel.frame = CGRectMake(cardX, cardY, cardWidth, 25);
    self.titleLabel.textColor = [UIColor whiteColor];
    [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview: self.titleLabel];
    
    // description label
    self.descriptionLabel = [UILabel new];
    self.descriptionLabel.frame = CGRectMake(0, self.view.frame.size.height-40, self.view.frame.size.width, 40);
    self.descriptionLabel.textColor = [UIColor whiteColor];
    self.descriptionLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    [self.descriptionLabel setTextAlignment: NSTextAlignmentCenter];
    [self.view addSubview: self.descriptionLabel];
    
    // status imageView
    self.statusImageView = [UIImageView new];
    self.statusImageView.frame = CGRectMake(cardX + cardWidth / 2 - 48, cardY + cardHeight / 2 - 48, 96, 96);
    [self.view addSubview: self.statusImageView];
    
    [self updateViewsForScannerStep];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

    UIEdgeInsets contentInset = UIEdgeInsetsMake(40, 20, 20, 20);
    CGSize viewSize = self.view.bounds.size;

    // preview view
    self.previewView.frame = CGRectMake(0, 0, viewSize.width, viewSize.height);

    // close button
    CGFloat closeButtonSize = 40.0f;
    CGRect closeButtonFrame = CGRectMake(viewSize.width - contentInset.right - closeButtonSize, contentInset.right, closeButtonSize, closeButtonSize);
    self.closeButton.frame = closeButtonFrame;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [UIView animateWithDuration:3 animations:^{
        self.closeButton.hidden = NO;
    }];

    // capture session
    [self setupCaptureSession];

}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self stopCameraSession];
}

- (void)stopCameraSession {
    // stop camera session
    [self removeInputAndOutput];
    [self.captureSession stopRunning];
}

#pragma mark - Actions

- (IBAction)suspend:(id)sender {

    // close view
    if ([self.delegate respondsToSelector:@selector(scannerSuspendedByUserAction)]) {
        [self.delegate scannerSuspendedByUserAction];
    }

}

- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer {

    CGPoint devicePoint = [self.videoPreviewLayer captureDevicePointOfInterestForPoint:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [self focusWithMode:AVCaptureFocusModeAutoFocus exposeWithMode:AVCaptureExposureModeAutoExpose atDevicePoint:devicePoint monitorSubjectAreaChange:YES];
}

- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    dispatch_async( videoDataOutputQueue, ^{
        AVCaptureDevice *device = self.input.device;
        NSError *error = nil;
        if ( [device lockForConfiguration:&error] ) {
            /*
             Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
             Call set(Focus/Exposure)Mode() to apply the new point of interest.
             */
            if ( device.isFocusPointOfInterestSupported && [device isFocusModeSupported:focusMode] ) {
                device.focusPointOfInterest = point;
                device.focusMode = focusMode;
            }

            if ( device.isExposurePointOfInterestSupported && [device isExposureModeSupported:exposureMode] ) {
                device.exposurePointOfInterest = point;
                device.exposureMode = exposureMode;
            }

            device.subjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange;
            [device unlockForConfiguration];
        }
        else {
            NSLog( @"Could not lock device for configuration: %@", error );
        }
    } );
}

- (void)setupCaptureSession {

    self.captureSession = [AVCaptureSession new];
    self.captureSession.sessionPreset = AVCaptureSessionPreset1280x720;

    self.backCamera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (!self.backCamera) {
        NSLog(@"Unable to access back camera!");
        return;
    }

    if ([self.backCamera hasTorch]) {
        [self.backCamera lockForConfiguration:nil];
        [self.backCamera setTorchMode:AVCaptureTorchModeAuto];  // use AVCaptureTorchModeOff to turn off
        [self.backCamera unlockForConfiguration];
    }

    if ([self.backCamera isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
        [self.backCamera lockForConfiguration:nil];
        CGPoint autofocusPoint = CGPointMake(0.5f, 0.5f);
        [self.backCamera setFocusPointOfInterest:autofocusPoint];
        [self.backCamera setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
        [self.backCamera unlockForConfiguration];
    }

    NSError *error;
    self.input = [AVCaptureDeviceInput deviceInputWithDevice:self.backCamera error:&error];
    if (!error) {

        if ([self.captureSession canAddInput:self.input]) {
            [self.captureSession addInput:self.input];
        }

        [self createVideoDataOutput];

        [self setupLivePreview];

    }
    else {
        NSLog(@"Error Unable to initialize back camera: %@", error.localizedDescription);
    }
}

- (void)removeInputAndOutput {
    [self.captureSession removeInput:self.input];
    [self.videoDataOutput setSampleBufferDelegate:nil queue:NULL];
    [self.captureSession removeOutput:self.videoDataOutput];
}

- (void)updateCardBorderColor:(const std::vector<vdoc::grab::Region> &)regions {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (regions.size() == 0) {
            [self.whiteView.cardLayer setStrokeColor: [UIColor whiteColor].CGColor];
        } else {
            [self.whiteView.cardLayer setStrokeColor: [UIColor greenColor].CGColor];
        }
    });
}

- (CGRect)offsetRectWithRect:(CGRect)rect {

    CGRect innerRect = CGRectInset(rect, rect.size.width*0.05, rect.size.height*0.05);
    CGFloat minSize = MIN(innerRect.size.width, innerRect.size.height);

    if (innerRect.size.width != minSize) {
        innerRect.origin.x += (innerRect.size.width - minSize) / 2;
        innerRect.size.width = minSize / ID_ASPECT_RATIO;
        innerRect.size.height = minSize;
    }
    else if (innerRect.size.height != minSize) {
        innerRect.origin.y += (innerRect.size.height - minSize) / 2;
        innerRect.size.height = minSize / ID_ASPECT_RATIO;
        innerRect.size.width = minSize;
    }

    CGRect offsetRect = CGRectOffset(innerRect, 0, 0);

    return offsetRect;
}

- (void)setupLivePreview {

    self.videoPreviewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];

    if (self.videoPreviewLayer) {

        self.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspect;
        self.videoPreviewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
        [self.previewView.layer addSublayer:self.videoPreviewLayer];

        dispatch_queue_t globalQueue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        dispatch_async(globalQueue, ^{

            [self.captureSession startRunning];

            dispatch_async(dispatch_get_main_queue(), ^{
                self.videoPreviewLayer.frame = self.previewView.bounds;
            });
        });
    }
}

- (void)createVideoDataOutput {

    // Make a video data output
    self.videoDataOutput = [AVCaptureVideoDataOutput new];

    // In grayscale mode we want YUV (YpCbCr 4:2:0) so we can directly access the graylevel intensity values (Y component)
    // In color mode we, BGRA format is used
    OSType format = self.grayscaleMode ? kCVPixelFormatType_420YpCbCr8BiPlanarFullRange : kCVPixelFormatType_32BGRA;

    self.videoDataOutput.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithUnsignedInt:format] forKey:(id)kCVPixelBufferPixelFormatTypeKey];

    // discard if the data output queue is blocked (as we process the still image)
    [self.videoDataOutput setAlwaysDiscardsLateVideoFrames:YES];

    if ( [self.captureSession canAddOutput:self.videoDataOutput] ) {
        [self.captureSession addOutput:self.videoDataOutput];
    }
    
    [[self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo] setEnabled:YES];

    // set default FPS
    if ([self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo].supportsVideoMinFrameDuration) {
        [self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo].videoMinFrameDuration = CMTimeMake(1, self.defaultFPS);
    }
    if ([self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo].supportsVideoMaxFrameDuration) {
        [self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo].videoMaxFrameDuration = CMTimeMake(1, self.defaultFPS);
    }

    // set default video orientation
    if ([self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo].supportsVideoOrientation) {
        [self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo].videoOrientation = self.defaultAVCaptureVideoOrientation;
    }

    // create a serial dispatch queue used for the sample buffer delegate as well as when a still image is captured
    // a serial dispatch queue must be used to guarantee that video frames will be delivered in order
    // see the header doc for setSampleBufferDelegate:queue: for more information
    videoDataOutputQueue = dispatch_queue_create("VideoDataOutputQueue", DISPATCH_QUEUE_SERIAL);
    [self.videoDataOutput setSampleBufferDelegate:self queue:videoDataOutputQueue];
}

- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(nonnull CMSampleBufferRef)sampleBuffer fromConnection:(nonnull AVCaptureConnection *)connection {

    // convert from Core Media to Core Video
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer, 0);

    void* bufferAddress;
    size_t width;
    size_t height;
    size_t bytesPerRow;

    CGContextRef context;

    int format_opencv;

    OSType format = CVPixelBufferGetPixelFormatType(imageBuffer);
    if (format == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) {

        format_opencv = CV_8UC1;

        bufferAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
        width = CVPixelBufferGetWidthOfPlane(imageBuffer, 0);
        height = CVPixelBufferGetHeightOfPlane(imageBuffer, 0);
        bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0);

    } else { // expect kCVPixelFormatType_32BGRA

        format_opencv = CV_8UC4;

        bufferAddress = CVPixelBufferGetBaseAddress(imageBuffer);
        width = CVPixelBufferGetWidth(imageBuffer);
        height = CVPixelBufferGetHeight(imageBuffer);
        bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);

    }

    // delegate image processing to the delegate
    cv::Mat image(height, width, format_opencv, bufferAddress, bytesPerRow);

    cv::Mat cropped;

    CGFloat bufferWidth = CVPixelBufferGetWidth(imageBuffer);
    CGFloat bufferHeight = CVPixelBufferGetHeight(imageBuffer);

    CGRect rect = CGRectMake(0, 0, bufferWidth, bufferHeight);
    CGRect innerRect = CGRectInset(rect, rect.size.width * 0.05, rect.size.height * 0.05);

    CGFloat minSize = MIN(innerRect.size.width, innerRect.size.height);

    if (innerRect.size.width != minSize) {
        innerRect.origin.x += (innerRect.size.width - minSize) / 2;
        innerRect.size.width = minSize;
        innerRect.size.height = minSize/ID_ASPECT_RATIO;
    }
    else if (innerRect.size.height != minSize) {
        innerRect.origin.y += (innerRect.size.height - minSize) / 2;
        innerRect.size.height = minSize/ID_ASPECT_RATIO;
        innerRect.size.width = minSize;
    }

    image(cv::Rect(innerRect.origin.x, innerRect.origin.y, innerRect.size.width, innerRect.size.height)).copyTo(cropped);


    croppedWidth = cropped.size().width;
    croppedHeight = cropped.size().height;
    
    [self updateCardBorderColor: self.video_adapter->getRegions()];
    
    int statusCode = [self detect:cropped];
    
    NSDictionary * result = statusCode == 0 ? [self extract] : nil;
    
    if (statusCode >= 0) {
        
        dispatch_after(
            dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC),
            dispatch_get_main_queue(),
            ^{
                [self setCameraCaptureCallback:statusCode ForResult:result];
            }
        );
        self.video_adapter->reset();
    }

    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
}

- (void) setCameraCaptureCallback:(int) statusCode ForResult:(NSDictionary *) result {
    
    switch (statusCode) {
        
        case 0 :
            [self handleResult: result];
            break;
        case 1 :
            [self.delegate onErrorCallback:statusCode withMessage:@"PROVIDED IMAGE IS TOO BLURRY!"];
            break;
        case 2 :
            [self.delegate onErrorCallback:statusCode withMessage:@"DOCUMENT NOT FOUND!"];
            break;
        case 3 :
            [self.delegate onErrorCallback:statusCode withMessage:@"OPERATION NOT SUPPORTED!"];
            break;
        default :
            break;
    }

}

- (int) detect:(cv::Mat &) mat {
    
    try {
        return self.video_adapter->nextImage(0, mat) ? 0 : -1;
    }
    catch (vdoc::BlurryImageException &e) {
        NSLog(@"[std::exception] %s", e.what());
        return 1;
    }
    catch (vdoc::DocumentNotFoundException &e) {
        NSLog(@"[vdoc::DocumentNotFoundException] %s", e.what());
        return 2;
    }
    catch (std::exception &e) {
        NSLog(@"[std::exception] %s", e.what());
        return 3;
    }
    
    return -1;
}

- (NSDictionary *) extract {
    if ( self.video_adapter->extract() ) {
        return [self extractWith:self.video_adapter->getResult()];
    }
    return nil;
}

- (NSDictionary *) extractWith:(const std::shared_ptr<vdoc::VisualDocument> &) vis_doc {
    
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
    
    vdoc::DocType document_type = vis_doc->getDocumentType();
    int doc_type = (int) document_type;
    
    std::string doc_type_key = "doc_type";
    NSString *key = [NSString stringWithCString:doc_type_key.c_str() encoding:NSUTF8StringEncoding];
    NSString *value = [NSString stringWithCString:std::to_string(doc_type).c_str() encoding:NSUTF8StringEncoding];
    [dictionary setObject:value forKey:key];
    
    if (document_type == vdoc::DocType::KZ_ID_4_FRONT || document_type == vdoc::DocType::KZ_ID_3_FRONT)
    {
        self.front_vis_doc = vis_doc;
    }
    else if (document_type == vdoc::DocType::KZ_ID_4_BACK || document_type == vdoc::DocType::KZ_ID_3_BACK)
    {
        self.back_vis_doc = vis_doc;
    }
    
    for (std::pair<std::string, std::tuple<int, std::string, double>> entry : vis_doc->getPredictedResult())
    {
        std::string c_key = entry.first;
        std::string c_value = vis_doc->getPredictedResult(c_key);
        
        NSString * key = [NSString stringWithCString:c_key.c_str() encoding:NSUTF8StringEncoding];
        NSString * value = [NSString stringWithCString:c_value.c_str() encoding:NSUTF8StringEncoding];
        
        [dictionary setObject:value forKey:key];
    }
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

- (BOOL) checkFrontSideFields {
    if (!self.front_vis_doc)
        return false;
    
    return vdoc::id::CheckFrontSideFields(self.front_vis_doc);
}

- (BOOL) checkBackSideFields {
    if (!self.back_vis_doc)
        return false;
    
    return vdoc::id::CheckBackSideFields(self.back_vis_doc);
}

- (BOOL) checkTwoSideConformity {
    if (!self.front_vis_doc)
        return false;
    
    return vdoc::id::CheckTwoSideConformity(self.front_vis_doc, self.back_vis_doc);
}

- (void) updateViewsForScannerStep {
    if (self.currentStep == 1) {
        self.titleLabel.text = @"Лицевая сторона";
        self.descriptionLabel.text = @"Наведите на лицевую сторону документа";
    } else {
        self.titleLabel.text = @"Обратная сторона";
        self.descriptionLabel.text = @"Наведите на обратную сторону документа";
    }
}

- (void) showStatusMessage:(NSString *) text {
    self.descriptionLabel.text = text;
}

- (void) showStatusImage:(UIImage *) image {
    [self.statusImageView setImage: image];
}

- (void) handleResult:(NSDictionary *) result {
    NSBundle *veridocBundle = [NSBundle bundleForClass:[self class]];
    int docType = [result[@"doc_type"] integerValue];
    
    if (self.currentStep == 1) {
        if (docType == 11 || docType == 14) { // front side
            if ([self checkFrontSideFields]) {
                UIImage *tickIcon = [UIImage imageNamed:@"tick_icon" inBundle:veridocBundle compatibleWithTraitCollection:nil];
                [self showStatusImage: tickIcon];
                [self showStatusMessage: @"Теперь покажите другую сторону"];
                [self stopCameraForSomeTime:^{
                    self.frontSideResult = result;
                    self.currentStep = 2;
                }];
                return;
            } else {
                UIImage *errorIcon = [UIImage imageNamed:@"error_icon" inBundle:veridocBundle compatibleWithTraitCollection:nil];
                [self showStatusImage: errorIcon];
                [self showStatusMessage: @"Ошибка"];
                [self stopCameraForSomeTime:^{}];
                return;
            }
        } else {
            UIImage *errorIcon = [UIImage imageNamed:@"error_icon" inBundle:veridocBundle compatibleWithTraitCollection:nil];
            [self showStatusImage: errorIcon];
            [self showStatusMessage: @"Вам нужно показать лицевую сторону"];
            [self stopCameraForSomeTime:^{}];
            return;
        }
    } else {
        if (docType == 12 || docType == 15) { // back side
            if ([self checkBackSideFields]) {
                if ([self checkTwoSideConformity]) {
                    UIImage *tickIcon = [UIImage imageNamed:@"tick_icon" inBundle:veridocBundle compatibleWithTraitCollection:nil];
                    [self showStatusImage: tickIcon];
                    [self showStatusMessage: @"Распознавание выполнено успешно"];
                    [self stopCameraForSomeTime:^{
                        self.backSideResult = result;
                        self.currentStep = 1;
                        
                        NSString *frontOriginalValue = [self.frontSideResult valueForKey: @"original_image"];
                        NSString *backOriginalValue = [self.backSideResult valueForKey: @"original_image"];
                        
                        NSMutableDictionary *allResults = [NSMutableDictionary dictionary];
                        [allResults addEntriesFromDictionary: self.frontSideResult];
                        [allResults addEntriesFromDictionary: self.backSideResult];
                        [allResults setValue: frontOriginalValue forKey: @"original_image_front"];
                        [allResults setValue: backOriginalValue forKey: @"original_image_back"];
                        [allResults removeObjectForKey: @"original_image"];
                        [self.delegate onSuccessCallback: allResults];
                    }];
                    return;
                } else {
                    UIImage *errorIcon = [UIImage imageNamed:@"error_icon" inBundle:veridocBundle compatibleWithTraitCollection:nil];
                    [self showStatusImage: errorIcon];
                    [self showStatusMessage: @"Ошибка. Начните все сначала"];
                    [self stopCameraForSomeTime:^{
                        self.currentStep = 1;
                    }];
                    return;
                }
            } else {
                UIImage *errorIcon = [UIImage imageNamed:@"error_icon" inBundle:veridocBundle compatibleWithTraitCollection:nil];
                [self showStatusImage: errorIcon];
                [self showStatusMessage: @"Ошибка"];
                [self stopCameraForSomeTime:^{}];
                return;
            }
        } else {
            UIImage *errorIcon = [UIImage imageNamed:@"error_icon" inBundle:veridocBundle compatibleWithTraitCollection:nil];
            [self showStatusImage: errorIcon];
            [self showStatusMessage: @"Вам нужно показать обратную сторону"];
            [self stopCameraForSomeTime:^{}];
            return;
        }
    }
}

- (void) stopCameraForSomeTime:(void (^)(void)) completion {
    [self.captureSession stopRunning];
    
    dispatch_after(
       dispatch_time(DISPATCH_TIME_NOW, 3.5 * NSEC_PER_SEC),
       dispatch_get_main_queue(),
       ^{
           [self.captureSession startRunning];
           [self showStatusImage: [UIImage imageNamed:@""]];
           [self updateViewsForScannerStep];
           completion();
       }
    );
}

@end
