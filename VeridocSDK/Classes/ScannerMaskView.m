//
//  ScannerMaskView.m
//  VeridocSDK
//
//  Created by Daulet Tungatarov on 8/18/19.
//  Copyright © 2019 Verigram. All rights reserved.
//

#import "ScannerMaskView.h"

#define LINE_WIDTH 3
#define ID_ASPECT_RATIO 1.586

@implementation ScannerMaskView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.cardFrame = CGRectZero;
        [self setupLayer];
    }
    return self;
}

-(void)setupLayer {
    self.whiteLayer = [CAShapeLayer new];
    [self.whiteLayer setOpaque: false];
    [self.whiteLayer setFillColor: [[UIColor whiteColor] colorWithAlphaComponent:0.5f].CGColor];
    [self.whiteLayer setFillRule: kCAFillRuleEvenOdd];
    
    self.cardLayer = [CAShapeLayer new];
    [self.cardLayer setFillColor: UIColor.clearColor.CGColor];
    [self.cardLayer setLineWidth: LINE_WIDTH];
    [self.cardLayer setStrokeColor: UIColor.whiteColor.CGColor];
    
    [self.layer addSublayer:self.whiteLayer];
    [self.layer addSublayer:self.cardLayer];
}

- (void)layoutSubviews {
    self.whiteLayer.frame = self.layer.bounds;
    self.cardLayer.frame = self.layer.bounds;
    
    CGRect cardFrame = [self offsetRectWithRect: self.frame];
    UIBezierPath *cardPath = [UIBezierPath bezierPathWithRoundedRect: cardFrame cornerRadius: 8];
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRect: self.layer.bounds];
    [path appendPath: cardPath];
    [path closePath];
    [self.whiteLayer setPath: path.CGPath];
    [self.cardLayer setPath: cardPath.CGPath];
}

- (CGRect)offsetRectWithRect:(CGRect)rect {
    
    CGRect innerRect = CGRectInset(rect, rect.size.width*0.05, rect.size.height*0.05);
    CGFloat minSize = MIN(innerRect.size.width, innerRect.size.height);
    
    if (innerRect.size.width != minSize) {
        innerRect.origin.x += (innerRect.size.width - minSize) / 2;
        innerRect.size.width = minSize / ID_ASPECT_RATIO;
        innerRect.size.height = minSize;
    }
    else if (innerRect.size.height != minSize) {
        innerRect.origin.y += (innerRect.size.height - minSize) / 2;
        innerRect.size.height = minSize / ID_ASPECT_RATIO;
        innerRect.size.width = minSize;
    }
    
    CGRect offsetRect = CGRectOffset(innerRect, 0, 0);
    
    return offsetRect;
}

@end
