//
//  ScannerMaskView.h
//  VeridocSDK
//
//  Created by Daulet Tungatarov on 8/18/19.
//  Copyright © 2019 Verigram. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ScannerMaskView : UIView

@property (nonatomic, strong) CAShapeLayer *whiteLayer;
@property (nonatomic, strong) CAShapeLayer *cardLayer;
@property (nonatomic, assign) CGRect cardFrame;

@end

NS_ASSUME_NONNULL_END
