#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface UIImage (Veridoc)
+(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat;
+ (cv::Mat)matFromBuffer:(CMSampleBufferRef)buffer;
+ (cv::Mat)toCvMat:(UIImage *)image;
+ (UIImage *)imageFromMat:(cv::Mat)image;
+ (UIImage *)fromCvMat:(cv::Mat)cvMat;
+ (UIImage *)croppedImageFromImage:(UIImage *)imageToCrop toRect:(CGRect)rect;
+ (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (UIImage *)imageFromImageView:(UIImageView *)imageView rectFrame:(CGRect)frame;
- (cv::Mat)cvMatRepresentationGray;
- (cv::Mat)cvMatRepresentationColor;
@end
