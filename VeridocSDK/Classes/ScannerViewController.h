#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DocumentType) {
    IdentityDocument,
    MRZ,
    BeelineSimCard,
};

@protocol ScannerViewControllerDelegate <NSObject>

- (void) onSuccessCallback:(NSDictionary *) result;

- (void) onErrorCallback:(int) statusCode withMessage:(NSString *) message;

- (void) scannerSuspendedByUserAction;

@end

@interface ScannerViewController : UIViewController

@property (nonatomic, weak) id <ScannerViewControllerDelegate> delegate;
@property (nonatomic, assign) DocumentType type;
@end
